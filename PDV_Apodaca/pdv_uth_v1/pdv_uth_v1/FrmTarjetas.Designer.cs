﻿namespace pdv_uth_v1
{
    partial class FrmTarjetas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTarjetas));
            this.vigenciaMes = new System.Windows.Forms.Label();
            this.numAutorizacion = new System.Windows.Forms.Label();
            this.vigenciaanio = new System.Windows.Forms.Label();
            this.txtMes = new System.Windows.Forms.TextBox();
            this.txtAutorizacion = new System.Windows.Forms.TextBox();
            this.txtAnio = new System.Windows.Forms.TextBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // vigenciaMes
            // 
            this.vigenciaMes.AutoSize = true;
            this.vigenciaMes.Location = new System.Drawing.Point(108, 61);
            this.vigenciaMes.Name = "vigenciaMes";
            this.vigenciaMes.Size = new System.Drawing.Size(71, 13);
            this.vigenciaMes.TabIndex = 0;
            this.vigenciaMes.Text = "Vigencia Mes";
            // 
            // numAutorizacion
            // 
            this.numAutorizacion.AutoSize = true;
            this.numAutorizacion.Location = new System.Drawing.Point(89, 158);
            this.numAutorizacion.Name = "numAutorizacion";
            this.numAutorizacion.Size = new System.Drawing.Size(122, 13);
            this.numAutorizacion.TabIndex = 1;
            this.numAutorizacion.Text = "Numero De Autorizacion";
            // 
            // vigenciaanio
            // 
            this.vigenciaanio.AutoSize = true;
            this.vigenciaanio.Location = new System.Drawing.Point(108, 110);
            this.vigenciaanio.Name = "vigenciaanio";
            this.vigenciaanio.Size = new System.Drawing.Size(70, 13);
            this.vigenciaanio.TabIndex = 2;
            this.vigenciaanio.Text = "Vigencia Año";
            // 
            // txtMes
            // 
            this.txtMes.Location = new System.Drawing.Point(92, 87);
            this.txtMes.Name = "txtMes";
            this.txtMes.Size = new System.Drawing.Size(100, 20);
            this.txtMes.TabIndex = 3;
            // 
            // txtAutorizacion
            // 
            this.txtAutorizacion.Location = new System.Drawing.Point(92, 174);
            this.txtAutorizacion.Name = "txtAutorizacion";
            this.txtAutorizacion.Size = new System.Drawing.Size(100, 20);
            this.txtAutorizacion.TabIndex = 4;
            // 
            // txtAnio
            // 
            this.txtAnio.Location = new System.Drawing.Point(92, 135);
            this.txtAnio.Name = "txtAnio";
            this.txtAnio.Size = new System.Drawing.Size(100, 20);
            this.txtAnio.TabIndex = 5;
            // 
            // btnEnviar
            // 
            this.btnEnviar.Location = new System.Drawing.Point(92, 214);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(100, 22);
            this.btnEnviar.TabIndex = 6;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // FrmTarjetas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(269, 303);
            this.Controls.Add(this.btnEnviar);
            this.Controls.Add(this.txtAnio);
            this.Controls.Add(this.txtAutorizacion);
            this.Controls.Add(this.txtMes);
            this.Controls.Add(this.vigenciaanio);
            this.Controls.Add(this.numAutorizacion);
            this.Controls.Add(this.vigenciaMes);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmTarjetas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmTarjetas";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label vigenciaMes;
        private System.Windows.Forms.Label numAutorizacion;
        private System.Windows.Forms.Label vigenciaanio;
        private System.Windows.Forms.TextBox txtMes;
        private System.Windows.Forms.TextBox txtAutorizacion;
        private System.Windows.Forms.TextBox txtAnio;
        private System.Windows.Forms.Button btnEnviar;
    }
}