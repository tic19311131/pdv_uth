﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmComprobantesUsuarios : Form
    {
        public FrmComprobantesUsuarios()
        {
            InitializeComponent();
        }
        public static string comprobanteINE;
        public static string comprobanteDOM;
        public static string comprobanteCURP;

        private void btnDom_Click(object sender, EventArgs e)
        {
            OpenFileDialog btnDom = new OpenFileDialog();
            //Filtros para abrir imagen
            btnDom.Filter = "Png Image (.png)|*.png|Bitmap Image (.bmp)|*.bmp|JPEG Image (.jpeg)|*.jpeg|Tiff Image (.tiff)|*.tiff";
            btnDom.Title = "Abrir Imagen para Producto";
            btnDom.DefaultExt = ".png";
            //si se abre imagen, se toma archivo
            if (btnDom.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = btnDom.FileName;
                //se carga archivo por su nombre al picBox
                picBoxComDom.Image = Image.FromFile(btnDom.FileName);
                //se toma el nmbre original
                lblComDom.Text = btnDom.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblComDom.Text.Substring(lblComDom.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblComDomListo.Text = string.Format(@"Domicilio_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void btnIne_Click(object sender, EventArgs e)
        {
            OpenFileDialog btnIne = new OpenFileDialog();
            //Filtros para abrir imagen
            btnIne.Filter = "Png Image (.png)|*.png|Bitmap Image (.bmp)|*.bmp|JPEG Image (.jpeg)|*.jpeg|Tiff Image (.tiff)|*.tiff";
            btnIne.Title = "Abrir Imagen para Producto";
            btnIne.DefaultExt = ".png";
            //si se abre imagen, se toma archivo
            if (btnIne.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = btnIne.FileName;
                //se carga archivo por su nombre al picBox
                picBoxComIne.Image = Image.FromFile(btnIne.FileName);
                //se toma el nmbre original
                lblComIne.Text = btnIne.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblComIne.Text.Substring(lblComIne.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblComIneListo.Text = string.Format(@"INE_{0}." + extension, DateTime.Now.Ticks);
            }

        }

        private void btnCurp_Click(object sender, EventArgs e)
        {
            OpenFileDialog btnIne = new OpenFileDialog();
            //Filtros para abrir imagen
            btnIne.Filter = "Png Image (.png)|*.png|Bitmap Image (.bmp)|*.bmp|JPEG Image (.jpeg)|*.jpeg|Tiff Image (.tiff)|*.tiff";
            btnIne.Title = "Abrir Imagen para Producto";
            btnIne.DefaultExt = ".png";
            //si se abre imagen, se toma archivo
            if (btnIne.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = btnIne.FileName;
                //se carga archivo por su nombre al picBox
                lblComCurp.Image = Image.FromFile(btnIne.FileName);
                //se toma el nmbre original
                lblComCurp.Text = btnIne.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblComCurp.Text.Substring(lblComCurp.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblComCurpListo.Text = string.Format(@"INE_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog btnIne = new OpenFileDialog();
            //Filtros para abrir imagen
            btnIne.Filter = "Png Image (.png)|*.png|Bitmap Image (.bmp)|*.bmp|JPEG Image (.jpeg)|*.jpeg|Tiff Image (.tiff)|*.tiff";
            btnIne.Title = "Abrir Imagen para Producto";
            btnIne.DefaultExt = ".png";
            //si se abre imagen, se toma archivo
            if (btnIne.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = btnIne.FileName;
                //se carga archivo por su nombre al picBox
                picBoxEstudios.Image = Image.FromFile(btnIne.FileName);
                //se toma el nmbre original
                lblCertEst.Text = btnIne.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblCertEst.Text.Substring(lblCertEst.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblCertEstListos.Text = string.Format(@"CertEstudios_{0}." + extension, DateTime.Now.Ticks);
            }
        }

        private void btnDomListo_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            picBoxComDom.Show();
            picBoxComCurp.Hide();
            picBoxComIne.Hide();
            picBoxEstudios.Hide();
        }

        private void btnCurpListo_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            picBoxComDom.Hide();
            picBoxComCurp.Show();
            picBoxComIne.Hide();
            picBoxEstudios.Hide();
        }

        private void btnIneListo_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            picBoxComDom.Hide();
            picBoxComCurp.Hide();
            picBoxComIne.Show();
            picBoxEstudios.Hide();
        }

        private void btnCompEstudios_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            picBoxComDom.Hide();
            picBoxComCurp.Hide();
            picBoxComIne.Hide();
            picBoxEstudios.Show();
        }

        private void btnRetroceder_Click(object sender, EventArgs e)
        {
            FrmUsuarios.comprobanteCURP = lblComCurpListo.Text;
            FrmUsuarios.comprobanteDOM = lblComDomListo.Text;
            FrmUsuarios.comprobanteINE = lblComIneListo.Text;
            FrmUsuarios.comprobanteEstudios = lblCertEstListos.Text;
            this.Close();
        }
    }
}
