﻿namespace pdv_uth_v1
{
    partial class FrmComprobantesUsuarios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmComprobantesUsuarios));
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnCompEstudios = new System.Windows.Forms.Button();
            this.btnEstudios = new System.Windows.Forms.Button();
            this.btnIne = new System.Windows.Forms.Button();
            this.btnCurp = new System.Windows.Forms.Button();
            this.btnDom = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnCurpListo = new System.Windows.Forms.Button();
            this.btnDomListo = new System.Windows.Forms.Button();
            this.btnIneListo = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCertEst = new System.Windows.Forms.Label();
            this.lblCertEstListos = new System.Windows.Forms.Label();
            this.lblComCurpListo = new System.Windows.Forms.Label();
            this.lblComDomListo = new System.Windows.Forms.Label();
            this.lblComIneListo = new System.Windows.Forms.Label();
            this.lblComCurp = new System.Windows.Forms.Label();
            this.lblComIne = new System.Windows.Forms.Label();
            this.lblComDom = new System.Windows.Forms.Label();
            this.btnRetroceder = new System.Windows.Forms.Button();
            this.picBoxComCurp = new System.Windows.Forms.PictureBox();
            this.picBoxComDom = new System.Windows.Forms.PictureBox();
            this.picBoxComIne = new System.Windows.Forms.PictureBox();
            this.picBoxEstudios = new System.Windows.Forms.PictureBox();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComCurp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComDom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComIne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxEstudios)).BeginInit();
            this.SuspendLayout();
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Gray;
            this.panel5.Controls.Add(this.btnCompEstudios);
            this.panel5.Controls.Add(this.btnEstudios);
            this.panel5.Controls.Add(this.btnIne);
            this.panel5.Controls.Add(this.btnCurp);
            this.panel5.Controls.Add(this.btnDom);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Controls.Add(this.btnCurpListo);
            this.panel5.Controls.Add(this.btnDomListo);
            this.panel5.Controls.Add(this.btnIneListo);
            this.panel5.Controls.Add(this.panel1);
            this.panel5.Controls.Add(this.picBoxComCurp);
            this.panel5.Controls.Add(this.picBoxComDom);
            this.panel5.Controls.Add(this.picBoxComIne);
            this.panel5.Controls.Add(this.picBoxEstudios);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(828, 436);
            this.panel5.TabIndex = 51;
            // 
            // btnCompEstudios
            // 
            this.btnCompEstudios.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCompEstudios.BackColor = System.Drawing.Color.Gray;
            this.btnCompEstudios.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCompEstudios.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCompEstudios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCompEstudios.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCompEstudios.ForeColor = System.Drawing.Color.Black;
            this.btnCompEstudios.Image = ((System.Drawing.Image)(resources.GetObject("btnCompEstudios.Image")));
            this.btnCompEstudios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCompEstudios.Location = new System.Drawing.Point(654, 324);
            this.btnCompEstudios.Name = "btnCompEstudios";
            this.btnCompEstudios.Size = new System.Drawing.Size(136, 41);
            this.btnCompEstudios.TabIndex = 71;
            this.btnCompEstudios.Text = "Comprobante\r\n            Estudios\r\n";
            this.btnCompEstudios.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCompEstudios.UseVisualStyleBackColor = false;
            this.btnCompEstudios.Click += new System.EventHandler(this.btnCompEstudios_Click);
            // 
            // btnEstudios
            // 
            this.btnEstudios.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEstudios.BackColor = System.Drawing.Color.Gray;
            this.btnEstudios.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnEstudios.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnEstudios.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEstudios.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEstudios.ForeColor = System.Drawing.Color.Black;
            this.btnEstudios.Image = ((System.Drawing.Image)(resources.GetObject("btnEstudios.Image")));
            this.btnEstudios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEstudios.Location = new System.Drawing.Point(22, 325);
            this.btnEstudios.Name = "btnEstudios";
            this.btnEstudios.Size = new System.Drawing.Size(116, 40);
            this.btnEstudios.TabIndex = 70;
            this.btnEstudios.Text = "Comprabante Estudios";
            this.btnEstudios.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEstudios.UseVisualStyleBackColor = false;
            this.btnEstudios.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnIne
            // 
            this.btnIne.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnIne.BackColor = System.Drawing.Color.Gray;
            this.btnIne.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnIne.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnIne.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIne.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIne.ForeColor = System.Drawing.Color.Black;
            this.btnIne.Image = ((System.Drawing.Image)(resources.GetObject("btnIne.Image")));
            this.btnIne.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIne.Location = new System.Drawing.Point(22, 185);
            this.btnIne.Name = "btnIne";
            this.btnIne.Size = new System.Drawing.Size(116, 40);
            this.btnIne.TabIndex = 69;
            this.btnIne.Text = "Comprabante INE";
            this.btnIne.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIne.UseVisualStyleBackColor = false;
            this.btnIne.Click += new System.EventHandler(this.btnIne_Click);
            // 
            // btnCurp
            // 
            this.btnCurp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCurp.BackColor = System.Drawing.Color.Gray;
            this.btnCurp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCurp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCurp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCurp.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCurp.ForeColor = System.Drawing.Color.Black;
            this.btnCurp.Image = ((System.Drawing.Image)(resources.GetObject("btnCurp.Image")));
            this.btnCurp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCurp.Location = new System.Drawing.Point(22, 251);
            this.btnCurp.Name = "btnCurp";
            this.btnCurp.Size = new System.Drawing.Size(116, 40);
            this.btnCurp.TabIndex = 68;
            this.btnCurp.Text = "Comprabante CURP";
            this.btnCurp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCurp.UseVisualStyleBackColor = false;
            this.btnCurp.Click += new System.EventHandler(this.btnCurp_Click);
            // 
            // btnDom
            // 
            this.btnDom.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDom.BackColor = System.Drawing.Color.Gray;
            this.btnDom.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDom.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnDom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDom.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDom.ForeColor = System.Drawing.Color.Black;
            this.btnDom.Image = ((System.Drawing.Image)(resources.GetObject("btnDom.Image")));
            this.btnDom.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDom.Location = new System.Drawing.Point(22, 107);
            this.btnDom.Name = "btnDom";
            this.btnDom.Size = new System.Drawing.Size(116, 40);
            this.btnDom.TabIndex = 67;
            this.btnDom.Text = "Comprabante Domicilio";
            this.btnDom.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDom.UseVisualStyleBackColor = false;
            this.btnDom.Click += new System.EventHandler(this.btnDom_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(173, 73);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(455, 334);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 66;
            this.pictureBox1.TabStop = false;
            // 
            // btnCurpListo
            // 
            this.btnCurpListo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCurpListo.BackColor = System.Drawing.Color.Gray;
            this.btnCurpListo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCurpListo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCurpListo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCurpListo.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCurpListo.ForeColor = System.Drawing.Color.Black;
            this.btnCurpListo.Image = ((System.Drawing.Image)(resources.GetObject("btnCurpListo.Image")));
            this.btnCurpListo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCurpListo.Location = new System.Drawing.Point(654, 250);
            this.btnCurpListo.Name = "btnCurpListo";
            this.btnCurpListo.Size = new System.Drawing.Size(136, 41);
            this.btnCurpListo.TabIndex = 63;
            this.btnCurpListo.Text = "Comprobante\r\n               CURP\r\n";
            this.btnCurpListo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCurpListo.UseVisualStyleBackColor = false;
            this.btnCurpListo.Click += new System.EventHandler(this.btnCurpListo_Click);
            // 
            // btnDomListo
            // 
            this.btnDomListo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDomListo.BackColor = System.Drawing.Color.Gray;
            this.btnDomListo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDomListo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnDomListo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDomListo.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDomListo.ForeColor = System.Drawing.Color.Black;
            this.btnDomListo.Image = ((System.Drawing.Image)(resources.GetObject("btnDomListo.Image")));
            this.btnDomListo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDomListo.Location = new System.Drawing.Point(654, 106);
            this.btnDomListo.Name = "btnDomListo";
            this.btnDomListo.Size = new System.Drawing.Size(136, 41);
            this.btnDomListo.TabIndex = 62;
            this.btnDomListo.Text = "Comprabante Domicilio";
            this.btnDomListo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDomListo.UseVisualStyleBackColor = false;
            this.btnDomListo.Click += new System.EventHandler(this.btnDomListo_Click);
            // 
            // btnIneListo
            // 
            this.btnIneListo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnIneListo.BackColor = System.Drawing.Color.Gray;
            this.btnIneListo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnIneListo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnIneListo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIneListo.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIneListo.ForeColor = System.Drawing.Color.Black;
            this.btnIneListo.Image = ((System.Drawing.Image)(resources.GetObject("btnIneListo.Image")));
            this.btnIneListo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIneListo.Location = new System.Drawing.Point(654, 184);
            this.btnIneListo.Name = "btnIneListo";
            this.btnIneListo.Size = new System.Drawing.Size(136, 41);
            this.btnIneListo.TabIndex = 61;
            this.btnIneListo.Text = "Comprobante\r\n               INE";
            this.btnIneListo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIneListo.UseVisualStyleBackColor = false;
            this.btnIneListo.Click += new System.EventHandler(this.btnIneListo_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblCertEst);
            this.panel1.Controls.Add(this.lblCertEstListos);
            this.panel1.Controls.Add(this.lblComCurpListo);
            this.panel1.Controls.Add(this.lblComDomListo);
            this.panel1.Controls.Add(this.lblComIneListo);
            this.panel1.Controls.Add(this.lblComCurp);
            this.panel1.Controls.Add(this.lblComIne);
            this.panel1.Controls.Add(this.lblComDom);
            this.panel1.Controls.Add(this.btnRetroceder);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(828, 67);
            this.panel1.TabIndex = 51;
            // 
            // lblCertEst
            // 
            this.lblCertEst.AutoSize = true;
            this.lblCertEst.Location = new System.Drawing.Point(529, 9);
            this.lblCertEst.Name = "lblCertEst";
            this.lblCertEst.Size = new System.Drawing.Size(74, 13);
            this.lblCertEst.TabIndex = 56;
            this.lblCertEst.Text = "CompEstudios";
            this.lblCertEst.Visible = false;
            // 
            // lblCertEstListos
            // 
            this.lblCertEstListos.AutoSize = true;
            this.lblCertEstListos.Location = new System.Drawing.Point(529, 43);
            this.lblCertEstListos.Name = "lblCertEstListos";
            this.lblCertEstListos.Size = new System.Drawing.Size(71, 13);
            this.lblCertEstListos.TabIndex = 55;
            this.lblCertEstListos.Text = "CompEsudios";
            this.lblCertEstListos.Visible = false;
            // 
            // lblComCurpListo
            // 
            this.lblComCurpListo.AutoSize = true;
            this.lblComCurpListo.Location = new System.Drawing.Point(399, 43);
            this.lblComCurpListo.Name = "lblComCurpListo";
            this.lblComCurpListo.Size = new System.Drawing.Size(56, 13);
            this.lblComCurpListo.TabIndex = 54;
            this.lblComCurpListo.Text = "CompCurp";
            this.lblComCurpListo.Visible = false;
            // 
            // lblComDomListo
            // 
            this.lblComDomListo.AutoSize = true;
            this.lblComDomListo.Location = new System.Drawing.Point(259, 43);
            this.lblComDomListo.Name = "lblComDomListo";
            this.lblComDomListo.Size = new System.Drawing.Size(56, 13);
            this.lblComDomListo.TabIndex = 53;
            this.lblComDomListo.Text = "CompDom";
            this.lblComDomListo.Visible = false;
            // 
            // lblComIneListo
            // 
            this.lblComIneListo.AutoSize = true;
            this.lblComIneListo.Location = new System.Drawing.Point(119, 43);
            this.lblComIneListo.Name = "lblComIneListo";
            this.lblComIneListo.Size = new System.Drawing.Size(52, 13);
            this.lblComIneListo.TabIndex = 52;
            this.lblComIneListo.Text = "CompINE";
            this.lblComIneListo.Visible = false;
            // 
            // lblComCurp
            // 
            this.lblComCurp.AutoSize = true;
            this.lblComCurp.Location = new System.Drawing.Point(399, 9);
            this.lblComCurp.Name = "lblComCurp";
            this.lblComCurp.Size = new System.Drawing.Size(56, 13);
            this.lblComCurp.TabIndex = 51;
            this.lblComCurp.Text = "CompCurp";
            this.lblComCurp.Visible = false;
            // 
            // lblComIne
            // 
            this.lblComIne.AutoSize = true;
            this.lblComIne.Location = new System.Drawing.Point(119, 9);
            this.lblComIne.Name = "lblComIne";
            this.lblComIne.Size = new System.Drawing.Size(52, 13);
            this.lblComIne.TabIndex = 50;
            this.lblComIne.Text = "CompINE";
            this.lblComIne.Visible = false;
            // 
            // lblComDom
            // 
            this.lblComDom.AutoSize = true;
            this.lblComDom.Location = new System.Drawing.Point(259, 9);
            this.lblComDom.Name = "lblComDom";
            this.lblComDom.Size = new System.Drawing.Size(56, 13);
            this.lblComDom.TabIndex = 49;
            this.lblComDom.Text = "CompDom";
            this.lblComDom.Visible = false;
            // 
            // btnRetroceder
            // 
            this.btnRetroceder.FlatAppearance.BorderSize = 0;
            this.btnRetroceder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetroceder.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetroceder.Image = ((System.Drawing.Image)(resources.GetObject("btnRetroceder.Image")));
            this.btnRetroceder.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnRetroceder.Location = new System.Drawing.Point(0, 0);
            this.btnRetroceder.Name = "btnRetroceder";
            this.btnRetroceder.Size = new System.Drawing.Size(74, 74);
            this.btnRetroceder.TabIndex = 48;
            this.btnRetroceder.UseVisualStyleBackColor = true;
            this.btnRetroceder.Click += new System.EventHandler(this.btnRetroceder_Click);
            // 
            // picBoxComCurp
            // 
            this.picBoxComCurp.Image = ((System.Drawing.Image)(resources.GetObject("picBoxComCurp.Image")));
            this.picBoxComCurp.Location = new System.Drawing.Point(173, 73);
            this.picBoxComCurp.Name = "picBoxComCurp";
            this.picBoxComCurp.Size = new System.Drawing.Size(455, 334);
            this.picBoxComCurp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxComCurp.TabIndex = 49;
            this.picBoxComCurp.TabStop = false;
            // 
            // picBoxComDom
            // 
            this.picBoxComDom.Image = ((System.Drawing.Image)(resources.GetObject("picBoxComDom.Image")));
            this.picBoxComDom.InitialImage = ((System.Drawing.Image)(resources.GetObject("picBoxComDom.InitialImage")));
            this.picBoxComDom.Location = new System.Drawing.Point(173, 73);
            this.picBoxComDom.Name = "picBoxComDom";
            this.picBoxComDom.Size = new System.Drawing.Size(455, 334);
            this.picBoxComDom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxComDom.TabIndex = 65;
            this.picBoxComDom.TabStop = false;
            // 
            // picBoxComIne
            // 
            this.picBoxComIne.Image = ((System.Drawing.Image)(resources.GetObject("picBoxComIne.Image")));
            this.picBoxComIne.Location = new System.Drawing.Point(173, 73);
            this.picBoxComIne.Name = "picBoxComIne";
            this.picBoxComIne.Size = new System.Drawing.Size(455, 334);
            this.picBoxComIne.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxComIne.TabIndex = 64;
            this.picBoxComIne.TabStop = false;
            // 
            // picBoxEstudios
            // 
            this.picBoxEstudios.Location = new System.Drawing.Point(173, 73);
            this.picBoxEstudios.Name = "picBoxEstudios";
            this.picBoxEstudios.Size = new System.Drawing.Size(455, 333);
            this.picBoxEstudios.TabIndex = 72;
            this.picBoxEstudios.TabStop = false;
            // 
            // FrmComprobantesUsuarios
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 436);
            this.Controls.Add(this.panel5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmComprobantesUsuarios";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ComprobantesUsuarios";
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComCurp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComDom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComIne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxEstudios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Button btnIne;
        public System.Windows.Forms.Button btnCurp;
        public System.Windows.Forms.Button btnDom;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Button btnCurpListo;
        public System.Windows.Forms.Button btnDomListo;
        public System.Windows.Forms.Button btnIneListo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblComCurpListo;
        private System.Windows.Forms.Label lblComDomListo;
        private System.Windows.Forms.Label lblComIneListo;
        private System.Windows.Forms.Label lblComCurp;
        private System.Windows.Forms.Label lblComIne;
        private System.Windows.Forms.Label lblComDom;
        private System.Windows.Forms.Button btnRetroceder;
        private System.Windows.Forms.PictureBox picBoxComCurp;
        private System.Windows.Forms.PictureBox picBoxComDom;
        private System.Windows.Forms.PictureBox picBoxComIne;
        public System.Windows.Forms.Button btnEstudios;
        private System.Windows.Forms.Label lblCertEst;
        private System.Windows.Forms.Label lblCertEstListos;
        public System.Windows.Forms.Button btnCompEstudios;
        private System.Windows.Forms.PictureBox picBoxEstudios;
    }
}