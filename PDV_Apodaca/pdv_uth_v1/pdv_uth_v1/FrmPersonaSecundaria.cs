﻿using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.clientes;
using Lib_pdv_uth_v1.usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmPersonaSecundaria : Form
    {
        PersonaSecundaria persec = new PersonaSecundaria();
        int idPerSec = 0;
        public static string comprobanteINE2;
        public static string comprobanteDOM2;
        public static string comprobanteCURP2;

        string idPersonaSecundaria = FrmCatalogoCliente.idParaPersona.ToString();
        public FrmPersonaSecundaria()
        {
            InitializeComponent();
        }
        public void habilitarFrom()
        {
            //habilitar forms
            txtNombre.Enabled = txtApPat.Enabled = txtApMat.Enabled = dtpFechaNacimiento.Enabled = txtCorreo.Enabled =
            txtCelular.Enabled = txtTelefono.Enabled = txtCalle.Enabled = txtNumCasa.Enabled =
            txtCP.Enabled = txtColonia.Enabled = txtFraccionamiento.Enabled = txtLocalidad.Enabled = txtMunicipio.Enabled =
            btnComprobantes.Enabled = txtCurp.Enabled = true;
            //change colors txt
            txtNombre.BackColor = txtApPat.BackColor = txtApMat.BackColor = txtCorreo.BackColor = txtCelular.BackColor =
            txtTelefono.BackColor = txtCalle.BackColor = txtNumCasa.BackColor = txtCP.BackColor = txtColonia.BackColor =
            txtFraccionamiento.BackColor = txtLocalidad.BackColor = txtMunicipio.BackColor = txtCurp.BackColor = Color.White;
        }
        public void limpiarForm()
        {
            //desabilitamos el form
            txtNombre.Enabled = txtApPat.Enabled = txtApMat.Enabled = dtpFechaNacimiento.Enabled = txtCorreo.Enabled =
            txtCelular.Enabled = txtTelefono.Enabled = txtCalle.Enabled = txtNumCasa.Enabled =
            txtCP.Enabled = txtColonia.Enabled = txtFraccionamiento.Enabled = txtLocalidad.Enabled = txtMunicipio.Enabled =
            btnComprobantes.Enabled = txtCurp.Enabled = false;
            //limpiamos el form
            txtNombre.Text = txtApPat.Text = txtApMat.Text = txtCorreo.Text = txtCelular.Text =
            txtTelefono.Text = txtCalle.Text = txtNumCasa.Text = txtCP.Text = txtColonia.Text =
            txtFraccionamiento.Text = txtLocalidad.Text = txtMunicipio.Text = txtCurp.Text = "";
            // cambiar el color de fondo
            txtNombre.BackColor = txtApPat.BackColor = txtApMat.BackColor = txtCorreo.BackColor = txtCelular.BackColor =
            txtTelefono.BackColor = txtCalle.BackColor = txtNumCasa.BackColor = txtCP.BackColor = txtColonia.BackColor =
            txtFraccionamiento.BackColor = txtLocalidad.BackColor = txtMunicipio.BackColor = txtCurp.BackColor = Color.DarkGray;

        }


        private void Cerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea cerrar la ventana de personas secundarias?",
                            "Cerrar",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnRetroceder_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCatalogoCliente frm = new FrmCatalogoCliente();
            frm.Show();
        }


        private void mostrarRegistrosEnDG()
        {
            dataGridPerSec.DataSource = null;
            //borrar todos los ren del DG
            dataGridPerSec.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dataGridPerSec.DataSource = persec.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dataGridPerSec.Refresh();
        }
        //Modificar Cliente
        private void btnModificarCliente_Click_1(object sender, EventArgs e)
        {
            //Datos Esta tomando el ultimo dato y lo sustituye en el sig add.
            habilitarFrom();
            PersonaSecundaria paraModif = new PersonaSecundaria();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();

            datos.Add(new DatosParaActualizar("nombre", txtNombre.Text));
            datos.Add(new DatosParaActualizar("apellido_paterno", txtApPat.Text));
            datos.Add(new DatosParaActualizar("apellido_materno", txtApMat.Text));
            datos.Add(new DatosParaActualizar("fecha_de_nacimiento", dtpFechaNacimiento.Value.Year + "-" + dtpFechaNacimiento.Value.Month + "-" + dtpFechaNacimiento.Value.Day));
            datos.Add(new DatosParaActualizar("celular", txtCelular.Text));
            datos.Add(new DatosParaActualizar("telefono", txtTelefono.Text));
            datos.Add(new DatosParaActualizar("correo", txtCorreo.Text));
            datos.Add(new DatosParaActualizar("calle", txtCalle.Text));
            datos.Add(new DatosParaActualizar("numero_casa", txtNumCasa.Text));
            datos.Add(new DatosParaActualizar("codigo_postal", txtCP.Text));
            datos.Add(new DatosParaActualizar("colonia", txtColonia.Text));
            datos.Add(new DatosParaActualizar("fraccionamiento", txtFraccionamiento.Text));
            datos.Add(new DatosParaActualizar("localidad", txtLocalidad.Text));
            datos.Add(new DatosParaActualizar("municipio", txtMunicipio.Text));
            datos.Add(new DatosParaActualizar("img_comprobante_domicilio", lblDomClientes.Text));
            datos.Add(new DatosParaActualizar("ine_comprobante", lblIneClientes.Text));
            datos.Add(new DatosParaActualizar("curp", txtCurp.Text));
            datos.Add(new DatosParaActualizar("curp_comprobante", lblCurpClientes.Text));

            if (paraModif.modificar(datos, idPerSec))
            {
                MessageBox.Show("cliente modificado");
                mostrarRegistrosEnDG();
            }
            else MessageBox.Show("Error cliente no modificado");

            habilitarFrom();
        }
        //Eliminar Cliente
        private void btnEliminarclientes_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente desea eliminar este cliente?", "Borrar cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (persec.eliminar(idPerSec))
                {
                    MessageBox.Show("Cliente eliminado ");
                }
                else MessageBox.Show("Error, Cliente no fue eliminado ");
            }
            mostrarRegistrosEnDG();
        }
        //Cerrar el programa
        private void btnApagar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea salir del programa?",
                    "Cerrando programa",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }
        //Abrir form para los comprobantes




        private void dataGridPerSec_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            idPerSec = int.Parse(dataGridPerSec.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dataGridPerSec.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApPat.Text = dataGridPerSec.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApMat.Text = dataGridPerSec.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtpFechaNacimiento.Value = DateTime.Parse(dataGridPerSec.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCorreo.Text = dataGridPerSec.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtCelular.Text = dataGridPerSec.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtTelefono.Text = dataGridPerSec.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtCalle.Text = dataGridPerSec.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtNumCasa.Text = dataGridPerSec.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtCP.Text = dataGridPerSec.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtColonia.Text = dataGridPerSec.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtFraccionamiento.Text = dataGridPerSec.Rows[e.RowIndex].Cells[12].Value.ToString();
            txtLocalidad.Text = dataGridPerSec.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtMunicipio.Text = dataGridPerSec.Rows[e.RowIndex].Cells[14].Value.ToString();
            lblDomClientes.Text = dataGridPerSec.Rows[e.RowIndex].Cells[15].Value.ToString();
            lblIneClientes.Text = dataGridPerSec.Rows[e.RowIndex].Cells[16].Value.ToString();
            txtCurp.Text = dataGridPerSec.Rows[e.RowIndex].Cells[17].Value.ToString();
            lblCurpClientes.Text = dataGridPerSec.Rows[e.RowIndex].Cells[18].Value.ToString();
            lblIDCliente.Text = dataGridPerSec.Rows[e.RowIndex].Cells[19].Value.ToString();
            habilitarFrom();
            btnModificarCliente.Enabled = true;
            btnEliminarcliente.Enabled = true;
            btnGuardarCliente.Enabled = false;
        }

        private void btnComprobantes_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            FrmComp frmComp = new FrmComp();
            frmComp.ShowDialog();
            lblDomClientes.Text = comprobanteDOM2;
            lblCurpClientes.Text = comprobanteCURP2;
            lblIneClientes.Text = comprobanteINE2;
            this.Show();
            MessageBox.Show(comprobanteCURP2 + "-" + comprobanteDOM2 + "-" + comprobanteINE2);
        }



        private void FrmPersonaSecundaria_Load(object sender, EventArgs e)
        {
            mostrarRegistrosEnDG();
            lblIDCliente.Text = FrmCatalogoCliente.idParaPersona.ToString();
            limpiarForm();
            btnModificarCliente.Enabled = false;
            btnEliminarcliente.Enabled = false;
            btnGuardarCliente.Enabled = false;
        }

        private void btnGuardarCliente_Click(object sender, EventArgs e)
        {
            //Hacemos una instancia de Persona alternativa
            PersonaSecundaria guardar = new PersonaSecundaria();
            guardar.Nombre = txtNombre.Text;
            guardar.ApellidoPaterno = txtApPat.Text;
            guardar.ApellidoMaterno = txtApMat.Text;
            guardar.FechaNacimiento = dtpFechaNacimiento.Value;
            guardar.Celular = txtCelular.Text;
            guardar.Telefono = txtTelefono.Text;
            guardar.Correo = txtCorreo.Text;
            guardar.Calle = txtCalle.Text;
            guardar.NumCasa = txtNumCasa.Text;
            guardar.CodigoPostal = txtCP.Text;
            guardar.Colonia = txtColonia.Text;
            guardar.Fraccionamiento = txtFraccionamiento.Text;
            guardar.Localidad = txtLocalidad.Text;
            guardar.Municipio = txtMunicipio.Text;
            guardar.ComproDomicilio = lblDomClientes.Text;
            guardar.ComprobanteINE = lblIneClientes.Text;
            guardar.Curp = txtCurp.Text;
            guardar.ComproCURP = lblCurpClientes.Text;
            guardar.idCliente = int.Parse(idPersonaSecundaria);
            if (guardar.alta())
            {

                MessageBox.Show("La persona <" + txtNombre.Text + "> Se ha registrado exitosamente. ", "Nueva persona registrada", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
                mostrarRegistrosEnDG();
                limpiarForm();
            }
            else
            {
                MessageBox.Show("Error al guardar al Usuario. " + PersonaSecundaria.msgError);
            }


        }

        private void btnAñadirClientes_Click(object sender, EventArgs e)
        {
            limpiarForm();
            habilitarFrom();
            btnGuardarCliente.Enabled = true;
            btnModificarCliente.Enabled = false;
            btnEliminarcliente.Enabled = false;
            btnGuardarCliente.Enabled = true;
        }

        private void btnModificarCliente_Click(object sender, EventArgs e)
        {
            //Datos Esta tomando el ultimo dato y lo sustituye en el sig add.
            habilitarFrom();
            PersonaSecundaria paraModif = new PersonaSecundaria();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();

            datos.Add(new DatosParaActualizar("nombre", txtNombre.Text));
            datos.Add(new DatosParaActualizar("apellido_paterno", txtApPat.Text));
            datos.Add(new DatosParaActualizar("apellido_materno", txtApMat.Text));
            datos.Add(new DatosParaActualizar("fecha_de_nacimiento", dtpFechaNacimiento.Value.Year + "-" + dtpFechaNacimiento.Value.Month + "-" + dtpFechaNacimiento.Value.Day));
            datos.Add(new DatosParaActualizar("celular", txtCelular.Text));
            datos.Add(new DatosParaActualizar("telefono", txtTelefono.Text));
            datos.Add(new DatosParaActualizar("correo", txtCorreo.Text));
            datos.Add(new DatosParaActualizar("calle", txtCalle.Text));
            datos.Add(new DatosParaActualizar("numero_casa", txtNumCasa.Text));
            datos.Add(new DatosParaActualizar("codigo_postal", txtCP.Text));
            datos.Add(new DatosParaActualizar("colonia", txtColonia.Text));
            datos.Add(new DatosParaActualizar("fraccionamiento", txtFraccionamiento.Text));
            datos.Add(new DatosParaActualizar("localidad", txtLocalidad.Text));
            datos.Add(new DatosParaActualizar("municipio", txtMunicipio.Text));
            datos.Add(new DatosParaActualizar("img_comprobante_domicilio", lblDomClientes.Text));
            datos.Add(new DatosParaActualizar("ine_comprobante", lblIneClientes.Text));
            datos.Add(new DatosParaActualizar("curp", txtCurp.Text));
            datos.Add(new DatosParaActualizar("curp_comprobante", lblCurpClientes.Text));

            if (paraModif.modificar(datos, idPerSec))
            {
                MessageBox.Show("cliente modificado");
                mostrarRegistrosEnDG();
            }
            else MessageBox.Show("Error cliente no modificado");

            habilitarFrom();
        }

        private void btnEliminarcliente_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente desea eliminar este cliente?", "Borrar cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (persec.eliminar(idPerSec))
                {
                    MessageBox.Show("Cliente eliminado ");
                }
                else MessageBox.Show("Error, Cliente no fue eliminado ");
            }
            mostrarRegistrosEnDG();
        }
    }
}
