﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmComp : Form
    {
        public FrmComp()
        {
            InitializeComponent();
        }
        //Variables para fotos de comprobantes Clientes
        private void btnRetroceder_Click(object sender, EventArgs e)
        {
            //Enviar nombres de comprobantes a clientes
            //comprobanteDomicilio = lblComDomListo.Text;
            //comprobanteCurp = lblComCurpListo.Text;
            //comprobanteIne = lblComIneListo.Text;
            FrmCatalogoCliente.comprobanteCURP = lblComCurpListo.Text;
            FrmCatalogoCliente.comprobanteDOM = lblComDomListo.Text;
            FrmCatalogoCliente.comprobanteINE = lblComIneListo.Text;
            FrmPersonaSecundaria.comprobanteCURP2 = lblComCurpListo.Text;
            FrmPersonaSecundaria.comprobanteDOM2 = lblComDomListo.Text;
            FrmPersonaSecundaria.comprobanteINE2 = lblComIneListo.Text;

            this.Close();
        }

        private void btnCurpListo_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            picBoxComDom.Hide();
            picBoxComCurp.Show();
            picBoxComIne.Hide();
        }

        private void btnIneListo_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            picBoxComDom.Hide();
            picBoxComCurp.Hide();
            picBoxComIne.Show();
        }

        private void btnDomListo_Click(object sender, EventArgs e)
        {
            pictureBox1.Hide();
            picBoxComDom.Show();
            picBoxComCurp.Hide();
            picBoxComIne.Hide();
        }
        private void btnDom_MouseClick(object sender, MouseEventArgs e)
        {
            OpenFileDialog btnDom = new OpenFileDialog();
            //Filtros para abrir imagen
            btnDom.Filter = "Png Image (.png)|*.png|Bitmap Image (.bmp)|*.bmp|JPEG Image (.jpeg)|*.jpeg|Tiff Image (.tiff)|*.tiff";
            btnDom.Title = "Abrir Imagen para Producto";
            btnDom.DefaultExt = ".png";
            //si se abre imagen, se toma archivo
            if (btnDom.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = btnDom.FileName;
                //se carga archivo por su nombre al picBox
                picBoxComDom.Image = Image.FromFile(btnDom.FileName);
                //se toma el nmbre original
                lblComDom.Text = btnDom.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblComDom.Text.Substring(lblComDom.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblComDomListo.Text = string.Format(@"Domicilio_{0}." + extension, DateTime.Now.Ticks);

            }




        }
        private void btnIne_Click(object sender, EventArgs e)
        {
            OpenFileDialog btnIne = new OpenFileDialog();
            //Filtros para abrir imagen
            btnIne.Filter = "Png Image (.png)|*.png|Bitmap Image (.bmp)|*.bmp|JPEG Image (.jpeg)|*.jpeg|Tiff Image (.tiff)|*.tiff";
            btnIne.Title = "Abrir Imagen para Producto";
            btnIne.DefaultExt = ".png";
            //si se abre imagen, se toma archivo
            if (btnIne.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = btnIne.FileName;
                //se carga archivo por su nombre al picBox
                picBoxComIne.Image = Image.FromFile(btnIne.FileName);
                //se toma el nmbre original
                lblComIne.Text = btnIne.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblComIne.Text.Substring(lblComIne.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblComIneListo.Text = string.Format(@"INE_{0}." + extension, DateTime.Now.Ticks);

            }
        }

        private void btnCurp_Click(object sender, EventArgs e)
        {
            OpenFileDialog btnCurp = new OpenFileDialog();
            //Filtros para abrir imagen
            btnCurp.Filter = "Png Image (.png)|*.png|Bitmap Image (.bmp)|*.bmp|JPEG Image (.jpeg)|*.jpeg|Tiff Image (.tiff)|*.tiff";
            btnCurp.Title = "Abrir Imagen para Producto";
            btnCurp.DefaultExt = ".png";
            //si se abre imagen, se toma archivo
            if (btnCurp.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = btnCurp.FileName;
                //se carga archivo por su nombre al picBox
                picBoxComCurp.Image = Image.FromFile(btnCurp.FileName);
                //se toma el nmbre original
                lblComCurp.Text = btnCurp.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblComCurp.Text.Substring(lblComCurp.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblComCurpListo.Text = string.Format(@"CURP_{0}." + extension, DateTime.Now.Ticks);

            }
        }



        private void btnDom_Click(object sender, EventArgs e)
        {
            OpenFileDialog btnDom = new OpenFileDialog();
            //Filtros para abrir imagen
            btnDom.Filter = "Png Image (.png)|*.png|Bitmap Image (.bmp)|*.bmp|JPEG Image (.jpeg)|*.jpeg|Tiff Image (.tiff)|*.tiff";
            btnDom.Title = "Abrir Imagen para Producto";
            btnDom.DefaultExt = ".png";
            //si se abre imagen, se toma archivo
            if (btnDom.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = btnDom.FileName;
                //se carga archivo por su nombre al picBox
                picBoxComDom.Image = Image.FromFile(btnDom.FileName);
                //se toma el nmbre original
                lblComDom.Text = btnDom.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblComDom.Text.Substring(lblComDom.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblComDomListo.Text = string.Format(@"Domicilio_{0}." + extension, DateTime.Now.Ticks);
            }


        }
    }
}
