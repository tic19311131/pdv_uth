﻿using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.cajas;
using Lib_pdv_uth_v1.Ventas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;



namespace pdv_uth_v1
{
    public partial class FrmVentas : Form
    {
       // public virtual void Sort(System.Collections.IComparer comparer);
        // MySqlConnection con = new MySqlConnection("server=127.0.0.1; Uid=root; Password=negro9720; port=pdv_uth_bd_v1");
        public FrmVentas()
        {
            InitializeComponent();
        }

         //borrar todos los ren del DG
        public void mostrarRegistrosEnDG()
        {
            Ventas venta = new Ventas();
            dataGridVentas.DataSource = null;
            //borrar todos los ren del DG 
            dataGridVentas.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dataGridVentas.DataSource = venta.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dataGridVentas.Refresh();
        }
        private void Cerrar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea cerrar la ventana de personas secundarias?",
                            "Cerrar",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnRetroceder_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmMenuPpal frm = new FrmMenuPpal();
            frm.Show();
        }

        private void dataGridVentas_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void FrmVentas_Load(object sender, EventArgs e)
        {
            btnEfectivo.Enabled = false;

            mostrarRegistrosEnDG();
            
        }
        
        public void mostrarRegistrosEnDGTarjeta()
        {
           
            VentasTarjeta venta = new VentasTarjeta();
            dataGridVentas.DataSource = null;
            //borrar todos los ren del DG 
            dataGridVentas.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dataGridVentas.DataSource = venta.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dataGridVentas.Refresh();
           // dataGridVentas.Sort(new DataRowComparer(SortOrder.Ascending));
        }

        private void pnlMetodos_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnEfectivo_Click(object sender, EventArgs e)
        {
            btnTarjeta.Enabled = true;
            btnEfectivo.Enabled = false;
            mostrarRegistrosEnDG();
        }

        private void btnTarjeta_Click(object sender, EventArgs e)
        {
            btnEfectivo.Enabled = true;
            btnTarjeta.Enabled = false;
            mostrarRegistrosEnDGTarjeta();
            
        }
    }
}
