﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Zen;
using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.clientes;

namespace pdv_uth_v1
{
    public partial class FrmCatalogoCliente : Form
    {
        //instanciar timer
        private Timer ti;
        //vars para acciones CRUD
        Cliente cli = new Cliente();
        int idCliente = 0;
        public static string comprobanteINE;
        public static string comprobanteDOM;
        public static string comprobanteCURP;
        public static int idParaPersona;

        public FrmCatalogoCliente()
        {
            ti = new Timer();
            ti.Tick += new EventHandler(eventTimer);
            InitializeComponent();
            ti.Enabled = true;
        }
        private void eventTimer(object ob, EventArgs e)
        {
            DateTime today = DateTime.Now;
            lblReloj.Text = today.ToString("hh:mm:ss tt");

        }

        private void FrmCatalogoCliente_Load(object sender, EventArgs e)
        {
            //Desabilitamos el formulario
            limpiarForm();
            //Mostrar los regristros del DataGrid
            mostrarRegistrosEnDG();
            //Poniendo nombre a los usuarios
            //lblNombreUsuario.Text = FrmLogin.usuario.Nombre + " " + FrmLogin.usuario.ApellidoMaterno;
            btnModificarCliente.Enabled = false;
            btnEliminarclientes.Enabled = false;
            btnGuardarCliente.Enabled = false;
            btnPersonasAlternativas.Enabled = false;

            
        }

        private void mostrarRegistrosEnDG()
        {
            //borrar todos los ren del DG
            if (dgClientes.Rows.Count < 0)
                dgClientes.Rows.Clear();

            //dgClientes.DataSource = null;

            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgClientes.DataSource = cli.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgClientes.Refresh();

        }


        private void dgClientes_CellContentClick_(object sender, DataGridViewCellEventArgs e)
        {
            idCliente = int.Parse(dgClientes.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgClientes.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApPat.Text = dgClientes.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApMat.Text = dgClientes.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtpFechaNacimiento.Value = DateTime.Parse(dgClientes.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCorreo.Text = dgClientes.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtCelular.Text = dgClientes.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtTelefono.Text = dgClientes.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtCalle.Text = dgClientes.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtNumCasa.Text = dgClientes.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtCP.Text = dgClientes.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtColonia.Text = dgClientes.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtFraccionamiento.Text = dgClientes.Rows[e.RowIndex].Cells[12].Value.ToString();
            txtLocalidad.Text = dgClientes.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtMunicipio.Text = dgClientes.Rows[e.RowIndex].Cells[14].Value.ToString();
            lblDomClientes.Text = dgClientes.Rows[e.RowIndex].Cells[15].Value.ToString();
            lblIneClientes.Text = dgClientes.Rows[e.RowIndex].Cells[16].Value.ToString();
            txtCurp.Text = dgClientes.Rows[e.RowIndex].Cells[17].Value.ToString();
            lblCurpClientes.Text = dgClientes.Rows[e.RowIndex].Cells[18].Value.ToString();
            idParaPersona = idCliente;
        }

        public void habilitarFrom()
        {
            //habilitar forms
            txtNombre.Enabled = txtApPat.Enabled = txtApMat.Enabled = dtpFechaNacimiento.Enabled = txtCorreo.Enabled =
            txtCelular.Enabled = txtTelefono.Enabled = txtCalle.Enabled = txtNumCasa.Enabled =
            txtCP.Enabled = txtColonia.Enabled = txtFraccionamiento.Enabled = txtLocalidad.Enabled = txtMunicipio.Enabled =
            btnComprobantes.Enabled = txtCurp.Enabled = true;


            //change colors txt
            txtNombre.BackColor = txtApPat.BackColor = txtApMat.BackColor = txtCorreo.BackColor = txtCelular.BackColor =
            txtTelefono.BackColor = txtCalle.BackColor = txtNumCasa.BackColor = txtCP.BackColor = txtColonia.BackColor =
            txtFraccionamiento.BackColor = txtLocalidad.BackColor = txtMunicipio.BackColor = txtCurp.BackColor = Color.White;
        }
        public void limpiarForm()
        {
            //desabilitamos el form
            txtNombre.Enabled = txtApPat.Enabled = txtApMat.Enabled = dtpFechaNacimiento.Enabled = txtCorreo.Enabled =
            txtCelular.Enabled = txtTelefono.Enabled = txtCalle.Enabled = txtNumCasa.Enabled =
            txtCP.Enabled = txtColonia.Enabled = txtFraccionamiento.Enabled = txtLocalidad.Enabled = txtMunicipio.Enabled =
            btnComprobantes.Enabled = txtCurp.Enabled = false;
            //limpiamos el form
            txtNombre.Text = txtApPat.Text = txtApMat.Text = txtCorreo.Text = txtCelular.Text =
            txtTelefono.Text = txtCalle.Text = txtNumCasa.Text = txtCP.Text = txtColonia.Text =
            txtFraccionamiento.Text = txtLocalidad.Text = txtMunicipio.Text = txtCurp.Text = "";
            // cambiar el color de fondo
            txtNombre.BackColor = txtApPat.BackColor = txtApMat.BackColor = txtCorreo.BackColor = txtCelular.BackColor =
            txtTelefono.BackColor = txtCalle.BackColor = txtNumCasa.BackColor = txtCP.BackColor = txtColonia.BackColor =
            txtFraccionamiento.BackColor = txtLocalidad.BackColor = txtMunicipio.BackColor = txtCurp.BackColor = Color.DarkGray;

        }
        private void btnAñadirClientes_Click(object sender, EventArgs e)
        {
            limpiarForm();
            habilitarFrom();
            btnModificarCliente.Enabled = false;
            btnEliminarclientes.Enabled = false;
            btnPersonasAlternativas.Enabled = false;
            btnGuardarCliente.Enabled = true;
            lblCurpClientes.Text = "Comprobante CURP";
            lblIneClientes.Text = "Comprobante INE";
            lblDomClientes.Text = "Comprobante Dom";
        }


        //Guardar Cliente
        private void btnGuardarCliente_Click_1(object sender, EventArgs e)
        {
            habilitarFrom();


            Cliente paraAlta = new Cliente();
            paraAlta.Nombre = txtNombre.Text;
            paraAlta.ApellidoPaterno = txtApPat.Text;
            paraAlta.ApellidoMaterno = txtApMat.Text;
            paraAlta.FechaNacimiento = dtpFechaNacimiento.Value;
            paraAlta.Celular = txtCelular.Text;
            paraAlta.Telefono = txtTelefono.Text;
            paraAlta.Correo = txtCorreo.Text;
            paraAlta.Domicilio.calle = txtCalle.Text;
            paraAlta.Domicilio.numero = txtNumCasa.Text;
            paraAlta.Domicilio.codigoPostal = txtCP.Text;
            paraAlta.Domicilio.colonia = txtColonia.Text;
            paraAlta.Domicilio.seccionFraccionamiento = txtFraccionamiento.Text;
            paraAlta.Domicilio.localidad = txtLocalidad.Text;
            paraAlta.Domicilio.municipio = txtMunicipio.Text;
            paraAlta.Domicilio.fotoComprobante = lblDomClientes.Text;
            paraAlta.ComprobanteINE = lblIneClientes.Text;
            paraAlta.Curp = txtCurp.Text;
            paraAlta.CurpCompro = lblCurpClientes.Text;
            if (paraAlta.alta())
            {
                MessageBox.Show("registro correcto");
                limpiarForm();
                mostrarRegistrosEnDG();
            }
            else
            {
                MessageBox.Show("Errror al guardar cliente. " + Cliente.msgError);
            }

        }
        //Modificar Cliente
        private void btnModificarCliente_Click_1(object sender, EventArgs e)
        {
            //Datos Esta tomando el ultimo dato y lo sustituye en el sig add.
            habilitarFrom();
            Cliente paraModif = new Cliente();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();

            datos.Add(new DatosParaActualizar("nombre", txtNombre.Text));
            datos.Add(new DatosParaActualizar("apellido_paterno", txtApPat.Text));
            datos.Add(new DatosParaActualizar("apellido_materno", txtApMat.Text));
            datos.Add(new DatosParaActualizar("fecha_de_nacimiento", dtpFechaNacimiento.Value.Year + "-" + dtpFechaNacimiento.Value.Month + "-" + dtpFechaNacimiento.Value.Day));
            datos.Add(new DatosParaActualizar("celular", txtCelular.Text));
            datos.Add(new DatosParaActualizar("telefono", txtTelefono.Text));
            datos.Add(new DatosParaActualizar("correo", txtCorreo.Text));
            datos.Add(new DatosParaActualizar("calle", txtCalle.Text));
            datos.Add(new DatosParaActualizar("numero_casa", txtNumCasa.Text));
            datos.Add(new DatosParaActualizar("codigo_postal", txtCP.Text));
            datos.Add(new DatosParaActualizar("colonia", txtColonia.Text));
            datos.Add(new DatosParaActualizar("fraccionamiento", txtFraccionamiento.Text));
            datos.Add(new DatosParaActualizar("localidad", txtLocalidad.Text));
            datos.Add(new DatosParaActualizar("municipio", txtMunicipio.Text));
            datos.Add(new DatosParaActualizar("img_comprobante_domicilio", lblDomClientes.Text));
            datos.Add(new DatosParaActualizar("ine_comprobante", lblIneClientes.Text));
            datos.Add(new DatosParaActualizar("curp", txtCurp.Text));
            datos.Add(new DatosParaActualizar("curp_comprobante", lblCurpClientes.Text));

            if (paraModif.modificar(datos, idCliente))
            {
                MessageBox.Show("cliente modificado");
                mostrarRegistrosEnDG();
            }
            else MessageBox.Show("Error cliente no modificado");

            habilitarFrom();
        }
        //Eliminar Cliente
        private void btnEliminarclientes_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente desea eliminar este cliente?", "Borrar cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (cli.eliminar(idCliente))
                {
                    MessageBox.Show("Cliente eliminado ");
                }
                else MessageBox.Show("Error, Cliente no fue eliminado ");
            }
            mostrarRegistrosEnDG();
        }
        //Cerrar el programa
        private void btnApagar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea salir del programa?",
                    "Cerrando programa",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }
        //Abrir form para los comprobantes
        private void btnComprobantes_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmComp frmComp = new FrmComp();
            frmComp.ShowDialog();
            lblDomClientes.Text = comprobanteDOM;
            lblCurpClientes.Text = comprobanteCURP;
            lblIneClientes.Text = comprobanteINE;
            this.Show();
            MessageBox.Show(comprobanteCURP + "-" + comprobanteDOM + "-" + comprobanteINE);
        }

        private void btnPersonasAlternativas_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmPersonaSecundaria frm = new FrmPersonaSecundaria();
            frm.ShowDialog();
            this.Show();
        }

        private void dgClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            idCliente = int.Parse(dgClientes.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgClientes.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApPat.Text = dgClientes.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApMat.Text = dgClientes.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtpFechaNacimiento.Value = DateTime.Parse(dgClientes.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCorreo.Text = dgClientes.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtCelular.Text = dgClientes.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtTelefono.Text = dgClientes.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtCalle.Text = dgClientes.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtNumCasa.Text = dgClientes.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtCP.Text = dgClientes.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtColonia.Text = dgClientes.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtFraccionamiento.Text = dgClientes.Rows[e.RowIndex].Cells[12].Value.ToString();
            txtLocalidad.Text = dgClientes.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtMunicipio.Text = dgClientes.Rows[e.RowIndex].Cells[14].Value.ToString();
            lblDomClientes.Text = dgClientes.Rows[e.RowIndex].Cells[15].Value.ToString();
            lblIneClientes.Text = dgClientes.Rows[e.RowIndex].Cells[16].Value.ToString();
            txtCurp.Text = dgClientes.Rows[e.RowIndex].Cells[17].Value.ToString();
            lblCurpClientes.Text = dgClientes.Rows[e.RowIndex].Cells[18].Value.ToString();
            idParaPersona = idCliente;
            btnModificarCliente.Enabled = true;
            btnEliminarclientes.Enabled = true;
            btnGuardarCliente.Enabled = false;
            btnPersonasAlternativas.Enabled = true;
            habilitarFrom();
        }

        //Botones para los FORMS
        private void btnProductos_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCatalogoProductos frm = new FrmCatalogoProductos();
            frm.Show();
        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmVentas frm = new FrmVentas();
            frm.Show();
        }

        private void btnCajas_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmCaja frm = new FrmCaja();
            frm.Show();
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmMenuPpal frm = new FrmMenuPpal();
            frm.Show();
        }
    }
}

