﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.productos;
using Zen;

namespace pdv_uth_v1
{
    public partial class FrmCatalogoProductos : Form
    {


        private Timer ti;
        //var de producto
        Producto prod = new Producto();
        int idProducto = 0;

        private void eventTimer(object ob, EventArgs e)
        {
            DateTime today = DateTime.Now;
            lblReloj.Text = today.ToString("hh:mm:ss tt");

        }

        public FrmCatalogoProductos()
        {
            ti = new Timer();
            ti.Tick += new EventHandler(eventTimer);
            ti.Enabled = true;
            InitializeComponent();
            mostrarRegistrosEnDG();
        }



        private void iconBtnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void FrmCatalogoProductos_Load(object sender, EventArgs e)
        {
            limpiarForm();
            //set del combo
            comboUnidadDeMedida.SelectedItem = comboUnidadDeMedida.Items[0];
        }


        private void icoBtnCargarImagen_Click(object sender, EventArgs e)
        {
            //Filtros para abrir imagen
            openFileDialogCargarImagen.Filter = "Image Files|*.png;";
            openFileDialogCargarImagen.Filter = "Png Image (.png)|*.png|Gif Image (.gif)|*.gif|JPEG Image (.jpeg)|*.jpeg|Tiff Image (.tiff)|*.tiff";
            openFileDialogCargarImagen.Title = "Abrir Imagen para Producto";
            openFileDialogCargarImagen.DefaultExt = ".png";
            //si se abre imagen, se toma archivo
            if (openFileDialogCargarImagen.ShowDialog() == DialogResult.OK)
            {
                //se toma nombre de archivo
                string fileName = openFileDialogCargarImagen.FileName;
                //se carga archivo por su nombre al picBox
                picBoxImagenProducto.Image = Image.FromFile(openFileDialogCargarImagen.FileName);
                //se toma el nmbre original
                lblImagenOriginal.Text = openFileDialogCargarImagen.SafeFileName;
                //calculamos la extension de la img, con los {ultimos 3 caracteres del nombre original
                string extension = lblImagenOriginal.Text.Substring(lblImagenOriginal.Text.Length - 3);
                //si es '.jpeg' va a decir 'peg', asi que acmpletamos
                extension = extension.ToLower() == "peg" ? "jpeg" : extension.ToLower();
                //se crea nombre nuevo UNICO para el Prodcuto, con el DateTime
                lblImagenAGuardar.Text = string.Format(@"prod_{0}." + extension, DateTime.Now.Ticks);

            }

        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                string codBarras = txtCodBarras.Text.Substring(0, txtCodBarras.Text.Length - 1);
                Zen.Barcode.CodeEan13BarcodeDraw barcode = Zen.Barcode.BarcodeDrawFactory.CodeEan13WithChecksum;
                var barcodeImage = barcode.Draw(codBarras, 80);
                var resultImage = new Bitmap(barcodeImage.Width, barcodeImage.Height + 20); // Acomodar el texto +20 de padding

                using (var graphics = Graphics.FromImage(resultImage))
                using (var font = new Font("Consolas", 12))//Tipografia de el numero
                using (var brush = new SolidBrush(Color.Black))//color del texto
                                                               //Alinear el texto
                using (var format = new StringFormat() { Alignment = StringAlignment.Center, LineAlignment = StringAlignment.Far })
                {
                    graphics.Clear(Color.White);
                    graphics.DrawImage(barcodeImage, 0, 0);
                    graphics.DrawString(txtCodBarras.Text, font, brush, resultImage.Width / 2, resultImage.Height, format);
                }
//picBoxCodBarras.Image = resultImage;
            }

        }

        private void txtCodBarras_TextChanged(object sender, EventArgs e)
        {

        }
        private void btnApagar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea salir del programa?",
                               "Cerrando programa",
                               MessageBoxButtons.YesNo,
                               MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }

        private void btnExpandirMn_Click(object sender, EventArgs e)
        {
            if (pnlNavbar.Width == 269)
            {
                //Quitar bordes al contraer menu
                pnlNavbar.Width = 69;
                btnHome.FlatAppearance.BorderSize = 0;
                //btnCompras.FlatAppearance.BorderSize = 0;
                btnProductos.FlatAppearance.BorderSize = 0;
                //btnCreditos.FlatAppearance.BorderSize = 0;
                btnVentas.FlatAppearance.BorderSize = 0;
                ipbLogo.Visible = false;
            }
            //activar bordes al expandir menu
            else
            {
                pnlNavbar.Width = 269;
                btnHome.FlatAppearance.BorderSize = 2;
                //btnCompras.FlatAppearance.BorderSize = 2;
                btnProductos.FlatAppearance.BorderSize = 2;
                //btnCreditos.FlatAppearance.BorderSize = 2;
                btnVentas.FlatAppearance.BorderSize = 2;
                ipbLogo.Visible = true;
            }
        }



        private void btnEliminarProducto_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente desea eliminar este Producto?", "Borrar producto", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (prod.eliminar(idProducto))
                {
                    MessageBox.Show("Producto eliminado ");
                    mostrarRegistrosEnDG();
                }
                else MessageBox.Show("Error, El Producto no fue eliminado ");
            }
        }


        private void btnAgregarProducto_Click(object sender, EventArgs e)
        {
            limpiarForm();
            habilitarFrom();
        }
        public void habilitarFrom()
        {
            //habilitar forms
            txtCodBarras.Enabled = txtNombre.Enabled = txtPrecio.Enabled = txtDescripcion.Enabled = comboUnidadDeMedida.Enabled =
            checkBoxEsPereceredo.Enabled = btnGuardarProducto.Enabled = true;
            //change colors txt
            txtCodBarras.BackColor = txtNombre.BackColor = txtPrecio.BackColor = txtDescripcion.BackColor = comboUnidadDeMedida.BackColor
               = Color.White;
        }
        public void limpiarForm()
        {
            //desabilitamos el form
            txtCodBarras.Enabled = txtNombre.Enabled = txtPrecio.Enabled = txtDescripcion.Enabled = comboUnidadDeMedida.Enabled =
               checkBoxEsPereceredo.Enabled = false;
            //limpiamos el form "";
            txtCodBarras.Text = txtNombre.Text = txtPrecio.Text = txtDescripcion.Text = lblImagenAGuardar.Text = lblImagenOriginal.Text = "";
            // cambiar el color de fondo
            txtCodBarras.BackColor = txtNombre.BackColor = txtPrecio.BackColor = txtDescripcion.BackColor = comboUnidadDeMedida.BackColor =
                 checkBoxEsPereceredo.BackColor = Color.DarkGray;
            picBoxImagenProducto.Image = null;
          //  picBoxCodBarras.Image = null;

        }



        private void btnModificarProducto_Click(object sender, EventArgs e)
        {
            Producto paraModif = new Producto();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();

            datos.Add(new DatosParaActualizar("nombre", txtNombre.Text));
            datos.Add(new DatosParaActualizar("descripcion", txtDescripcion.Text));
            datos.Add(new DatosParaActualizar("precio", txtPrecio.Text));
            datos.Add(new DatosParaActualizar("codigo_barras", txtCodBarras.Text));
            datos.Add(new DatosParaActualizar("imagen_producto", lblImagenAGuardar.Text));
            datos.Add(new DatosParaActualizar("unidad_medida", comboUnidadDeMedida.Text));
            datos.Add(new DatosParaActualizar("es_perecedero", checkBoxEsPereceredo.Checked ? "1" : "0"));

            if (paraModif.modificar(datos, idProducto))
            {
                // obtener el dir de la app
                string bin = System.IO.Path.GetDirectoryName(Application.StartupPath);
                //ir a un dir arriba
                string dir = System.IO.Path.GetDirectoryName(bin);
                //agregar el path para guardar la imagen
                dir += "\\Imagenes\\Productos\\";
                //guardar la imagen
                if (Directory.Exists(dir) == true)
                {
                    picBoxImagenProducto.Image.Save(dir + lblImagenAGuardar.Text);
                }
                MessageBox.Show("Producto modificado");
                mostrarRegistrosEnDG();
                limpiarForm();
            }
            else MessageBox.Show("Error, producto no modificado");
            {
                habilitarFrom();
            }

        }
        private void dgProductos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

            idProducto = int.Parse(dgProductos.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dgProductos.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtDescripcion.Text = dgProductos.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtPrecio.Text = double.Parse(dgProductos.Rows[e.RowIndex].Cells[3].Value.ToString()).ToString("F2", CultureInfo.CreateSpecificCulture("en-US"));
            txtCodBarras.Text = dgProductos.Rows[e.RowIndex].Cells[4].Value.ToString();
            lblImagenAGuardar.Text = lblImagenOriginal.Text = dgProductos.Rows[e.RowIndex].Cells[5].Value.ToString();
            comboUnidadDeMedida.Text = dgProductos.Rows[e.RowIndex].Cells[6].Value.ToString();
            checkBoxEsPereceredo.Checked = bool.Parse(dgProductos.Rows[e.RowIndex].Cells[7].Value.ToString());
            string bin = System.IO.Path.GetDirectoryName(Application.StartupPath);
            string dir = System.IO.Path.GetDirectoryName(bin);
            dir += "\\Imagenes\\Productos\\";
            string foto = dir + lblImagenAGuardar.Text;
            if (File.Exists(foto) == true)
            {

                picBoxImagenProducto.Image = Image.FromFile(foto);
            }
            else
            {
                picBoxImagenProducto.Image = null;
            }
            habilitarFrom();
            btnGuardarProducto.Enabled = false;

        }

        //string bin = Path.GetDirectoryName(Application.StartupPath);
        //string ruta = Path.GetDirectoryName(bin);
        //ruta += "\\Imagenes\\Usuarios\\";
        //    string foto = ruta + frmLogin.us.Imagen_persona;
        //pictureBox1.Image = Image.FromFile(foto)

        private void btnGuardarProducto_Click_1(object sender, EventArgs e)
        {
            //cargar todos los campos en producto
            prod = new Producto();
            prod.Nombre = txtNombre.Text;
            prod.Descripcion = txtDescripcion.Text;
            prod.Precio = double.Parse(txtPrecio.Text);
            prod.CodigoDeBarras = txtCodBarras.Text;
            prod.Imagen = lblImagenAGuardar.Text;
            prod.UnidadDeMedida = (UnidadDeMedida)Enum.Parse(typeof(UnidadDeMedida), comboUnidadDeMedida.SelectedItem.ToString());
            prod.EsPerecedero = checkBoxEsPereceredo.Checked;
            //hacer el insert con metodo alta()
            if (prod.alta())
            {

                // obtener el dir de la app

                string bin = System.IO.Path.GetDirectoryName(Application.StartupPath);
                //ir a un dir arriba
                string dir = System.IO.Path.GetDirectoryName(bin);
                //agregar el path para guardar la imagen
                dir += "\\Imagenes\\Productos\\";
                //guardar la imagen
                if (Directory.Exists(dir) == true)
                {
                    picBoxImagenProducto.Image.Save(dir + lblImagenAGuardar.Text);
                }

                MessageBox.Show("EL producto <" + txtNombre.Text + "> se ha almacenado.", "Nuevo Producto", MessageBoxButtons.OK, MessageBoxIcon.Information);
                limpiarForm();
                mostrarRegistrosEnDG();
            }
            else
                MessageBox.Show("Error, no se almacenó producto. " + Producto.msgError);

        }
        //Botones para los FORMS




        //Form Menu
        private void btnHome_Click(object sender, EventArgs e)
        {
            this.Hide();
            FrmMenuPpal frm = new FrmMenuPpal();
            frm.Show();

        }

        private void btnVentas_Click(object sender, EventArgs e)
        {
            //Form Ventas
            this.Hide();
            FrmVentas frm = new FrmVentas();
            frm.Show();


        }

        private void btnProductos_Click(object sender, EventArgs e)
        {
            //Form Prouctos
            this.Hide();
            FrmCatalogoProductos frm = new FrmCatalogoProductos();
            frm.Show();

        }

        private void btnCajas_Click(object sender, EventArgs e)
        {
            //Form Caja
            this.Hide();
            FrmCaja frm = new FrmCaja();
            frm.Show();


        }
    }
}