﻿namespace pdv_uth_v1
{
     public partial class FrmComp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmComp));
            this.picBoxComIne = new System.Windows.Forms.PictureBox();
            this.picBoxComDom = new System.Windows.Forms.PictureBox();
            this.picBoxComCurp = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblComCurpListo = new System.Windows.Forms.Label();
            this.lblComDomListo = new System.Windows.Forms.Label();
            this.lblComIneListo = new System.Windows.Forms.Label();
            this.lblComCurp = new System.Windows.Forms.Label();
            this.lblComIne = new System.Windows.Forms.Label();
            this.lblComDom = new System.Windows.Forms.Label();
            this.btnRetroceder = new System.Windows.Forms.Button();
            this.btnIneListo = new System.Windows.Forms.Button();
            this.btnDomListo = new System.Windows.Forms.Button();
            this.btnCurpListo = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnDom = new System.Windows.Forms.Button();
            this.btnCurp = new System.Windows.Forms.Button();
            this.btnIne = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComIne)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComDom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComCurp)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // picBoxComIne
            // 
            this.picBoxComIne.Image = ((System.Drawing.Image)(resources.GetObject("picBoxComIne.Image")));
            this.picBoxComIne.Location = new System.Drawing.Point(122, 73);
            this.picBoxComIne.Name = "picBoxComIne";
            this.picBoxComIne.Size = new System.Drawing.Size(416, 304);
            this.picBoxComIne.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxComIne.TabIndex = 64;
            this.picBoxComIne.TabStop = false;
            // 
            // picBoxComDom
            // 
            this.picBoxComDom.Image = ((System.Drawing.Image)(resources.GetObject("picBoxComDom.Image")));
            this.picBoxComDom.InitialImage = ((System.Drawing.Image)(resources.GetObject("picBoxComDom.InitialImage")));
            this.picBoxComDom.Location = new System.Drawing.Point(122, 72);
            this.picBoxComDom.Name = "picBoxComDom";
            this.picBoxComDom.Size = new System.Drawing.Size(416, 304);
            this.picBoxComDom.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxComDom.TabIndex = 65;
            this.picBoxComDom.TabStop = false;
            // 
            // picBoxComCurp
            // 
            this.picBoxComCurp.Image = ((System.Drawing.Image)(resources.GetObject("picBoxComCurp.Image")));
            this.picBoxComCurp.Location = new System.Drawing.Point(122, 72);
            this.picBoxComCurp.Name = "picBoxComCurp";
            this.picBoxComCurp.Size = new System.Drawing.Size(416, 304);
            this.picBoxComCurp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxComCurp.TabIndex = 49;
            this.picBoxComCurp.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblComCurpListo);
            this.panel1.Controls.Add(this.lblComDomListo);
            this.panel1.Controls.Add(this.lblComIneListo);
            this.panel1.Controls.Add(this.lblComCurp);
            this.panel1.Controls.Add(this.lblComIne);
            this.panel1.Controls.Add(this.lblComDom);
            this.panel1.Controls.Add(this.btnRetroceder);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(578, 67);
            this.panel1.TabIndex = 51;
            // 
            // lblComCurpListo
            // 
            this.lblComCurpListo.AutoSize = true;
            this.lblComCurpListo.Location = new System.Drawing.Point(399, 43);
            this.lblComCurpListo.Name = "lblComCurpListo";
            this.lblComCurpListo.Size = new System.Drawing.Size(56, 13);
            this.lblComCurpListo.TabIndex = 54;
            this.lblComCurpListo.Text = "CompCurp";
            this.lblComCurpListo.Visible = false;
            // 
            // lblComDomListo
            // 
            this.lblComDomListo.AutoSize = true;
            this.lblComDomListo.Location = new System.Drawing.Point(259, 43);
            this.lblComDomListo.Name = "lblComDomListo";
            this.lblComDomListo.Size = new System.Drawing.Size(56, 13);
            this.lblComDomListo.TabIndex = 53;
            this.lblComDomListo.Text = "CompDom";
            this.lblComDomListo.Visible = false;
            // 
            // lblComIneListo
            // 
            this.lblComIneListo.AutoSize = true;
            this.lblComIneListo.Location = new System.Drawing.Point(119, 43);
            this.lblComIneListo.Name = "lblComIneListo";
            this.lblComIneListo.Size = new System.Drawing.Size(52, 13);
            this.lblComIneListo.TabIndex = 52;
            this.lblComIneListo.Text = "CompINE";
            this.lblComIneListo.Visible = false;
            // 
            // lblComCurp
            // 
            this.lblComCurp.AutoSize = true;
            this.lblComCurp.Location = new System.Drawing.Point(399, 9);
            this.lblComCurp.Name = "lblComCurp";
            this.lblComCurp.Size = new System.Drawing.Size(56, 13);
            this.lblComCurp.TabIndex = 51;
            this.lblComCurp.Text = "CompCurp";
            this.lblComCurp.Visible = false;
            // 
            // lblComIne
            // 
            this.lblComIne.AutoSize = true;
            this.lblComIne.Location = new System.Drawing.Point(119, 9);
            this.lblComIne.Name = "lblComIne";
            this.lblComIne.Size = new System.Drawing.Size(52, 13);
            this.lblComIne.TabIndex = 50;
            this.lblComIne.Text = "CompINE";
            this.lblComIne.Visible = false;
            // 
            // lblComDom
            // 
            this.lblComDom.AutoSize = true;
            this.lblComDom.Location = new System.Drawing.Point(259, 9);
            this.lblComDom.Name = "lblComDom";
            this.lblComDom.Size = new System.Drawing.Size(56, 13);
            this.lblComDom.TabIndex = 49;
            this.lblComDom.Text = "CompDom";
            this.lblComDom.Visible = false;
            // 
            // btnRetroceder
            // 
            this.btnRetroceder.FlatAppearance.BorderSize = 0;
            this.btnRetroceder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetroceder.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetroceder.Image = ((System.Drawing.Image)(resources.GetObject("btnRetroceder.Image")));
            this.btnRetroceder.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnRetroceder.Location = new System.Drawing.Point(0, 0);
            this.btnRetroceder.Name = "btnRetroceder";
            this.btnRetroceder.Size = new System.Drawing.Size(74, 74);
            this.btnRetroceder.TabIndex = 48;
            this.btnRetroceder.UseVisualStyleBackColor = true;
            this.btnRetroceder.Click += new System.EventHandler(this.btnRetroceder_Click);
            // 
            // btnIneListo
            // 
            this.btnIneListo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnIneListo.BackColor = System.Drawing.Color.Gray;
            this.btnIneListo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnIneListo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnIneListo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIneListo.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIneListo.ForeColor = System.Drawing.Color.Black;
            this.btnIneListo.Image = ((System.Drawing.Image)(resources.GetObject("btnIneListo.Image")));
            this.btnIneListo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIneListo.Location = new System.Drawing.Point(262, 382);
            this.btnIneListo.Name = "btnIneListo";
            this.btnIneListo.Size = new System.Drawing.Size(134, 41);
            this.btnIneListo.TabIndex = 61;
            this.btnIneListo.Text = "Comprobante\r\n               INE";
            this.btnIneListo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIneListo.UseVisualStyleBackColor = false;
            this.btnIneListo.Click += new System.EventHandler(this.btnIneListo_Click);
            // 
            // btnDomListo
            // 
            this.btnDomListo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDomListo.BackColor = System.Drawing.Color.Gray;
            this.btnDomListo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDomListo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnDomListo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDomListo.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDomListo.ForeColor = System.Drawing.Color.Black;
            this.btnDomListo.Image = ((System.Drawing.Image)(resources.GetObject("btnDomListo.Image")));
            this.btnDomListo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDomListo.Location = new System.Drawing.Point(122, 382);
            this.btnDomListo.Name = "btnDomListo";
            this.btnDomListo.Size = new System.Drawing.Size(126, 41);
            this.btnDomListo.TabIndex = 62;
            this.btnDomListo.Text = "Comprabante Domicilio";
            this.btnDomListo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDomListo.UseVisualStyleBackColor = false;
            this.btnDomListo.Click += new System.EventHandler(this.btnDomListo_Click);
            // 
            // btnCurpListo
            // 
            this.btnCurpListo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCurpListo.BackColor = System.Drawing.Color.Gray;
            this.btnCurpListo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCurpListo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCurpListo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCurpListo.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCurpListo.ForeColor = System.Drawing.Color.Black;
            this.btnCurpListo.Image = ((System.Drawing.Image)(resources.GetObject("btnCurpListo.Image")));
            this.btnCurpListo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCurpListo.Location = new System.Drawing.Point(402, 382);
            this.btnCurpListo.Name = "btnCurpListo";
            this.btnCurpListo.Size = new System.Drawing.Size(136, 41);
            this.btnCurpListo.TabIndex = 63;
            this.btnCurpListo.Text = "Comprobante\r\n               CURP\r\n";
            this.btnCurpListo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCurpListo.UseVisualStyleBackColor = false;
            this.btnCurpListo.Click += new System.EventHandler(this.btnCurpListo_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(122, 74);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(416, 304);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 66;
            this.pictureBox1.TabStop = false;
            // 
            // btnDom
            // 
            this.btnDom.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnDom.BackColor = System.Drawing.Color.Gray;
            this.btnDom.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnDom.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnDom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDom.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDom.ForeColor = System.Drawing.Color.Black;
            this.btnDom.Image = ((System.Drawing.Image)(resources.GetObject("btnDom.Image")));
            this.btnDom.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDom.Location = new System.Drawing.Point(3, 113);
            this.btnDom.Name = "btnDom";
            this.btnDom.Size = new System.Drawing.Size(116, 40);
            this.btnDom.TabIndex = 67;
            this.btnDom.Text = "Comprabante Domicilio";
            this.btnDom.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnDom.UseVisualStyleBackColor = false;
            this.btnDom.Click += new System.EventHandler(this.btnDom_Click);
            this.btnDom.MouseClick += new System.Windows.Forms.MouseEventHandler(this.btnDom_MouseClick);
            // 
            // btnCurp
            // 
            this.btnCurp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnCurp.BackColor = System.Drawing.Color.Gray;
            this.btnCurp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCurp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnCurp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCurp.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCurp.ForeColor = System.Drawing.Color.Black;
            this.btnCurp.Image = ((System.Drawing.Image)(resources.GetObject("btnCurp.Image")));
            this.btnCurp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCurp.Location = new System.Drawing.Point(3, 320);
            this.btnCurp.Name = "btnCurp";
            this.btnCurp.Size = new System.Drawing.Size(116, 40);
            this.btnCurp.TabIndex = 68;
            this.btnCurp.Text = "Comprabante CURP";
            this.btnCurp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCurp.UseVisualStyleBackColor = false;
            this.btnCurp.Click += new System.EventHandler(this.btnCurp_Click);
            // 
            // btnIne
            // 
            this.btnIne.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnIne.BackColor = System.Drawing.Color.Gray;
            this.btnIne.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnIne.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnIne.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnIne.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnIne.ForeColor = System.Drawing.Color.Black;
            this.btnIne.Image = ((System.Drawing.Image)(resources.GetObject("btnIne.Image")));
            this.btnIne.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnIne.Location = new System.Drawing.Point(3, 219);
            this.btnIne.Name = "btnIne";
            this.btnIne.Size = new System.Drawing.Size(116, 40);
            this.btnIne.TabIndex = 69;
            this.btnIne.Text = "Comprabante INE";
            this.btnIne.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnIne.UseVisualStyleBackColor = false;
            this.btnIne.Click += new System.EventHandler(this.btnIne_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Gray;
            this.panel5.Controls.Add(this.btnIne);
            this.panel5.Controls.Add(this.btnCurp);
            this.panel5.Controls.Add(this.btnDom);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Controls.Add(this.btnCurpListo);
            this.panel5.Controls.Add(this.btnDomListo);
            this.panel5.Controls.Add(this.btnIneListo);
            this.panel5.Controls.Add(this.panel1);
            this.panel5.Controls.Add(this.picBoxComCurp);
            this.panel5.Controls.Add(this.picBoxComDom);
            this.panel5.Controls.Add(this.picBoxComIne);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(578, 434);
            this.panel5.TabIndex = 50;
            // 
            // FrmComp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 434);
            this.Controls.Add(this.panel5);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmComp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormComprobantes";
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComIne)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComDom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxComCurp)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxComIne;
        private System.Windows.Forms.PictureBox picBoxComDom;
        private System.Windows.Forms.PictureBox picBoxComCurp;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblComCurpListo;
        private System.Windows.Forms.Label lblComDomListo;
        private System.Windows.Forms.Label lblComIneListo;
        private System.Windows.Forms.Label lblComCurp;
        private System.Windows.Forms.Label lblComIne;
        private System.Windows.Forms.Label lblComDom;
        private System.Windows.Forms.Button btnRetroceder;
        public System.Windows.Forms.Button btnIneListo;
        public System.Windows.Forms.Button btnDomListo;
        public System.Windows.Forms.Button btnCurpListo;
        private System.Windows.Forms.PictureBox pictureBox1;
        public System.Windows.Forms.Button btnDom;
        public System.Windows.Forms.Button btnCurp;
        public System.Windows.Forms.Button btnIne;
        private System.Windows.Forms.Panel panel5;
    }
}