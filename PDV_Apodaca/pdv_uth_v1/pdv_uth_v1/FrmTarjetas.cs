﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    public partial class FrmTarjetas : Form
    {       



        public FrmTarjetas()
        {
            InitializeComponent();
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            FrmCaja.txtAnio = txtAnio.Text;
            FrmCaja.txtMes = txtMes.Text;
            FrmCaja.numAutorizacion = txtAutorizacion.Text;
            this.Close();
        }
    }
}
