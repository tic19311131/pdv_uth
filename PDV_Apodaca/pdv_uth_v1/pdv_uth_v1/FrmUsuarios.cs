﻿using Lib_pdv_uth_v1;
using Lib_pdv_uth_v1.usuarios;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace pdv_uth_v1
{
    //agregar referencias lib_pdv usuarios/personas
    public partial class FrmUsuarios : Form
    {
        public static string comprobanteINE;
        public static string comprobanteDOM;
        public static string comprobanteCURP;
        public static string comprobanteEstudios;
        int idUsuario = 0;
        public static int idParaPersona;
        Usuario usuario = new Usuario();
        public FrmUsuarios()
        {
            InitializeComponent();
        }


        private void btnApagar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Usted desea cerrar la ventana de personas secundarias?",
                            "Cerrar",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.Close();
            }
        }


        public void habilitarFrom()
        {
            //habilitar forms
            txtNombre.Enabled = txtApPat.Enabled = txtApMat.Enabled = dtpFechaNacimiento.Enabled = txtCorreo.Enabled =
            txtCelular.Enabled = txtTelefono.Enabled = txtCalle.Enabled = txtNumCasa.Enabled =
            txtCP.Enabled = txtColonia.Enabled = txtFraccionamiento.Enabled = txtLocalidad.Enabled = txtMunicipio.Enabled =
            btnComprobantes.Enabled = txtCurp.Enabled = txtActaDeNac.Enabled = txtCertEstudios.Enabled = txtNumSS.Enabled = cbTipoUser.Enabled = txtUsuarioContra.Enabled =
            txtComprobanteDomicilio.Enabled = txtComproINE.Enabled = txtComprobanteCurp.Enabled = true;
            //change colors txt
            txtNombre.BackColor = txtApPat.BackColor = txtApMat.BackColor = txtCorreo.BackColor = txtCelular.BackColor =
            txtTelefono.BackColor = txtCalle.BackColor = txtNumCasa.BackColor = txtCP.BackColor = txtColonia.BackColor =
            txtFraccionamiento.BackColor = txtLocalidad.BackColor = txtMunicipio.BackColor = txtCurp.BackColor = txtActaDeNac.BackColor = txtCertEstudios.BackColor = txtNumSS.BackColor = cbTipoUser.BackColor = txtUsuarioContra.BackColor =
            txtComprobanteDomicilio.BackColor = txtComproINE.BackColor = txtComprobanteCurp.BackColor = Color.White;
        }
        public void limpiarForm()
        {
            //desabilitamos el form
            txtNombre.Enabled = txtApPat.Enabled = txtApMat.Enabled = dtpFechaNacimiento.Enabled = txtCorreo.Enabled =
            txtCelular.Enabled = txtTelefono.Enabled = txtCalle.Enabled = txtNumCasa.Enabled =
            txtCP.Enabled = txtColonia.Enabled = txtFraccionamiento.Enabled = txtLocalidad.Enabled = txtMunicipio.Enabled =
            btnComprobantes.Enabled = txtCurp.Enabled = txtActaDeNac.Enabled = txtCertEstudios.Enabled = txtNumSS.Enabled = cbTipoUser.Enabled = txtUsuarioContra.Enabled =
            txtComprobanteDomicilio.Enabled = txtComproINE.Enabled = txtComprobanteCurp.Enabled = false;
            //limpiamos el form
            txtNombre.Text = txtApPat.Text = txtApMat.Text = txtCorreo.Text = txtCelular.Text =
            txtTelefono.Text = txtCalle.Text = txtNumCasa.Text = txtCP.Text = txtColonia.Text =
            txtFraccionamiento.Text = txtLocalidad.Text = txtMunicipio.Text = txtCurp.Text = txtActaDeNac.Text = txtCertEstudios.Text = txtNumSS.Text =  txtUsuarioContra.Text =
            txtComprobanteDomicilio.Text = txtComproINE.Text = txtComprobanteCurp.Text = "";
            // cambiar el color de fondo
            txtNombre.BackColor = txtApPat.BackColor = txtApMat.BackColor = txtCorreo.BackColor = txtCelular.BackColor =
            txtTelefono.BackColor = txtCalle.BackColor = txtNumCasa.BackColor = txtCP.BackColor = txtColonia.BackColor =
            txtFraccionamiento.BackColor = txtLocalidad.BackColor = txtMunicipio.BackColor = txtCurp.BackColor = txtActaDeNac.BackColor = txtCertEstudios.BackColor = txtNumSS.BackColor = cbTipoUser.BackColor = txtUsuarioContra.BackColor =
            txtComprobanteDomicilio.BackColor = txtComproINE.BackColor = txtComprobanteCurp.BackColor = Color.DarkGray;

        }
        
        private void btnAñadirClientes_Click(object sender, EventArgs e)
        {
            limpiarForm();
            habilitarFrom();
            btnGuardarCliente.Enabled = true;
            btnModificarCliente.Enabled = false;
            btnEliminarCliente.Enabled = false;
            btnGuardarCliente.Enabled = true;
        }


        public void mostrarRegistrosEnDG()
        {
            Usuario cli = new Usuario();
            dGridUsuarios.DataSource = null;
            //borrar todos los ren del DG 
            dGridUsuarios.Rows.Clear();
            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dGridUsuarios.DataSource = cli.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dGridUsuarios.Refresh();
        }

        private void btnRetroceder_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            FrmMenuPpal frm = new FrmMenuPpal();
            frm.Show();
        }



        private void FrmUsuarios_Load(object sender, EventArgs e)
        {
            mostrarRegistrosEnDG();
            limpiarForm();
            btnModificarCliente.Enabled = false;
            btnEliminarCliente.Enabled = false;
            btnGuardarCliente.Enabled = false;
        }

        private void btnGuardarCliente_Click(object sender, EventArgs e)
        {
            //Hacemos una instancia de Persona alternativa
            Usuario paraAlta = new Usuario();
            paraAlta.Nombre = txtNombre.Text;
            paraAlta.ApellidoPaterno = txtApPat.Text;
            paraAlta.ApellidoMaterno = txtApMat.Text;
            paraAlta.Celular = txtCelular.Text;
            paraAlta.Telefono = txtTelefono.Text;
            paraAlta.Correo = txtCorreo.Text;
            paraAlta.FechaNacimiento = dtpFechaNacimiento.Value;
            paraAlta.Domicilio.calle = txtCalle.Text;
            paraAlta.Domicilio.numero = txtNumCasa.Text;
            paraAlta.Domicilio.colonia = txtColonia.Text;
            paraAlta.Domicilio.seccionFraccionamiento = txtFraccionamiento.Text;
            paraAlta.Domicilio.codigoPostal = txtCP.Text;
            paraAlta.Domicilio.localidad = txtLocalidad.Text;
            paraAlta.Domicilio.municipio = txtMunicipio.Text;
            paraAlta.Domicilio.fotoComprobante = txtComprobanteDomicilio.Text;
            paraAlta.ComprobanteINE = txtComproINE.Text;
            paraAlta.Curp = txtCurp.Text;
            paraAlta.CurpCompro = txtComprobanteCurp.Text;
            paraAlta.ActaNacimientoComprobante = txtActaDeNac.Text;
            paraAlta.CertificadoEscolar = txtCertEstudios.Text;
            paraAlta.NumeroSeguroSocial = txtNumSS.Text;
            paraAlta.TipoUsuario = (TipoUsuario)Enum.Parse(typeof(TipoUsuario), cbTipoUser.SelectedItem.ToString());
            paraAlta.Contraseña = txtUsuarioContra.Text;

            if (paraAlta.alta())
            {

                MessageBox.Show("La persona <" + txtNombre.Text + "> Se ha registrado exitosamente. ", "Nuevo usuario registrado", MessageBoxButtons.OK, MessageBoxIcon.Information); ;
                mostrarRegistrosEnDG();
                limpiarForm();
            }
            else
            {
                MessageBox.Show("Error al guardar al Usuario. " + Usuario.msgError);
            }

        }

        private void btnModificarCliente_Click(object sender, EventArgs e)
        {
            //Datos Esta tomando el ultimo dato y lo sustituye en el sig add.
            habilitarFrom();
            Usuario paraModif = new Usuario();
            List<DatosParaActualizar> datos = new List<DatosParaActualizar>();

            datos.Add(new DatosParaActualizar("nombre", txtNombre.Text));
            datos.Add(new DatosParaActualizar("apellido_paterno", txtApPat.Text));
            datos.Add(new DatosParaActualizar("apellido_materno", txtApMat.Text));
            datos.Add(new DatosParaActualizar("fecha_de_nacimiento", dtpFechaNacimiento.Value.Year + "-" + dtpFechaNacimiento.Value.Month + "-" + dtpFechaNacimiento.Value.Day));
            datos.Add(new DatosParaActualizar("correo", txtCorreo.Text));
            datos.Add(new DatosParaActualizar("telefono", txtTelefono.Text));
            datos.Add(new DatosParaActualizar("celular", txtCelular.Text));
            datos.Add(new DatosParaActualizar("calle", txtCalle.Text));
            datos.Add(new DatosParaActualizar("numero_casa", txtNumCasa.Text));
            datos.Add(new DatosParaActualizar("codigo_postal", txtCP.Text));
            datos.Add(new DatosParaActualizar("colonia", txtColonia.Text));
            datos.Add(new DatosParaActualizar("fraccionamiento", txtFraccionamiento.Text));
            datos.Add(new DatosParaActualizar("localidad", txtLocalidad.Text));
            datos.Add(new DatosParaActualizar("municipio", txtMunicipio.Text));
            datos.Add(new DatosParaActualizar("img_comprobante_domicilio", txtComprobanteDomicilio.Text));
            datos.Add(new DatosParaActualizar("ine_comprobante", txtComproINE.Text));
            datos.Add(new DatosParaActualizar("curp", txtCurp.Text));
            datos.Add(new DatosParaActualizar("curp_comprobante", txtComprobanteCurp.Text));
            datos.Add(new DatosParaActualizar("acta_nacimiento", txtActaDeNac.Text));
            datos.Add(new DatosParaActualizar("certificado_estudios", txtCertEstudios.Text));
            datos.Add(new DatosParaActualizar("numero_seguro_social", txtNumSS.Text));
            datos.Add(new DatosParaActualizar("tipo_usuario", cbTipoUser.Text));
            datos.Add(new DatosParaActualizar("pwd", txtUsuarioContra.Text));
            if (paraModif.modificar(datos, idUsuario))
            {
                MessageBox.Show("usuario modificado");
                mostrarRegistrosEnDG();
            }
            else MessageBox.Show("Error usuario no modificado");

            habilitarFrom();
        }
        

        private void dGridUsuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            idUsuario = int.Parse(dGridUsuarios.Rows[e.RowIndex].Cells[0].Value.ToString());
            txtNombre.Text = dGridUsuarios.Rows[e.RowIndex].Cells[1].Value.ToString();
            txtApPat.Text = dGridUsuarios.Rows[e.RowIndex].Cells[2].Value.ToString();
            txtApMat.Text = dGridUsuarios.Rows[e.RowIndex].Cells[3].Value.ToString();
            dtpFechaNacimiento.Value = DateTime.Parse(dGridUsuarios.Rows[e.RowIndex].Cells[4].Value.ToString());
            txtCelular.Text = dGridUsuarios.Rows[e.RowIndex].Cells[5].Value.ToString();
            txtTelefono.Text = dGridUsuarios.Rows[e.RowIndex].Cells[6].Value.ToString();
            txtCorreo.Text = dGridUsuarios.Rows[e.RowIndex].Cells[7].Value.ToString();
            txtCalle.Text = dGridUsuarios.Rows[e.RowIndex].Cells[8].Value.ToString();
            txtNumCasa.Text = dGridUsuarios.Rows[e.RowIndex].Cells[9].Value.ToString();
            txtCP.Text = dGridUsuarios.Rows[e.RowIndex].Cells[10].Value.ToString();
            txtColonia.Text = dGridUsuarios.Rows[e.RowIndex].Cells[11].Value.ToString();
            txtFraccionamiento.Text = dGridUsuarios.Rows[e.RowIndex].Cells[12].Value.ToString();
            txtLocalidad.Text = dGridUsuarios.Rows[e.RowIndex].Cells[13].Value.ToString();
            txtMunicipio.Text = dGridUsuarios.Rows[e.RowIndex].Cells[14].Value.ToString();
            txtComprobanteDomicilio.Text = dGridUsuarios.Rows[e.RowIndex].Cells[15].Value.ToString();
            txtComproINE.Text = dGridUsuarios.Rows[e.RowIndex].Cells[16].Value.ToString();
            txtCurp.Text = dGridUsuarios.Rows[e.RowIndex].Cells[17].Value.ToString();
            txtComprobanteCurp.Text = dGridUsuarios.Rows[e.RowIndex].Cells[18].Value.ToString();
            txtActaDeNac.Text = dGridUsuarios.Rows[e.RowIndex].Cells[19].Value.ToString();
            txtCertEstudios.Text = dGridUsuarios.Rows[e.RowIndex].Cells[20].Value.ToString();
            txtNumSS.Text = dGridUsuarios.Rows[e.RowIndex].Cells[21].Value.ToString();
            cbTipoUser.Text = dGridUsuarios.Rows[e.RowIndex].Cells[22].Value.ToString();
            txtUsuarioContra.Text = dGridUsuarios.Rows[e.RowIndex].Cells[23].Value.ToString();
            btnModificarCliente.Enabled = true;
            btnEliminarCliente.Enabled = true;
            btnGuardarCliente.Enabled = false;
            habilitarFrom();
        }

        private void btnEliminarCliente_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Realmente desea eliminar este Usuario?", "Borrar cliente", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (usuario.eliminar(idUsuario))
                {
                    MessageBox.Show("Cliente eliminado ");
                }
                else MessageBox.Show("Error, Usuario no fue eliminado ");
            }
            mostrarRegistrosEnDG();
        }

        private void btnComprobantes_Click(object sender, EventArgs e)
        {
            FrmComprobantesUsuarios frm = new FrmComprobantesUsuarios();
            frm.ShowDialog();
            txtComprobanteDomicilio.Text = comprobanteDOM;
            txtComprobanteCurp.Text = comprobanteCURP;
            txtComproINE.Text = comprobanteINE;
            txtCertEstudios.Text = comprobanteEstudios;
            this.Show();
            MessageBox.Show(comprobanteCURP + " - " + comprobanteDOM + " - " + comprobanteINE+" - "+comprobanteEstudios);
        }
    }
}
