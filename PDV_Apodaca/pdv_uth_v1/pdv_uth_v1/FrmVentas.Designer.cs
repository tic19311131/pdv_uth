﻿namespace pdv_uth_v1
{
    partial class FrmVentas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmVentas));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlDgVentas = new System.Windows.Forms.Panel();
            this.dataGridVentas = new System.Windows.Forms.DataGridView();
            this.pnlMetodos = new System.Windows.Forms.Panel();
            this.btnRetroceder = new System.Windows.Forms.Button();
            this.btnEfectivo = new System.Windows.Forms.Button();
            this.btnTarjeta = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.pnlDgVentas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVentas)).BeginInit();
            this.pnlMetodos.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Gray;
            this.panel1.Controls.Add(this.pnlDgVentas);
            this.panel1.Controls.Add(this.pnlMetodos);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 450);
            this.panel1.TabIndex = 0;
            // 
            // pnlDgVentas
            // 
            this.pnlDgVentas.Controls.Add(this.dataGridVentas);
            this.pnlDgVentas.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlDgVentas.Location = new System.Drawing.Point(0, 80);
            this.pnlDgVentas.Name = "pnlDgVentas";
            this.pnlDgVentas.Size = new System.Drawing.Size(800, 370);
            this.pnlDgVentas.TabIndex = 1;
            // 
            // dataGridVentas
            // 
            this.dataGridVentas.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridVentas.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridVentas.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dataGridVentas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridVentas.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridVentas.Location = new System.Drawing.Point(0, 0);
            this.dataGridVentas.Name = "dataGridVentas";
            this.dataGridVentas.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridVentas.Size = new System.Drawing.Size(800, 370);
            this.dataGridVentas.TabIndex = 1;
            // 
            // pnlMetodos
            // 
            this.pnlMetodos.Controls.Add(this.btnTarjeta);
            this.pnlMetodos.Controls.Add(this.btnEfectivo);
            this.pnlMetodos.Controls.Add(this.btnRetroceder);
            this.pnlMetodos.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMetodos.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pnlMetodos.Location = new System.Drawing.Point(0, 0);
            this.pnlMetodos.Name = "pnlMetodos";
            this.pnlMetodos.Size = new System.Drawing.Size(800, 82);
            this.pnlMetodos.TabIndex = 0;
            this.pnlMetodos.Paint += new System.Windows.Forms.PaintEventHandler(this.pnlMetodos_Paint);
            // 
            // btnRetroceder
            // 
            this.btnRetroceder.FlatAppearance.BorderSize = 0;
            this.btnRetroceder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRetroceder.Font = new System.Drawing.Font("Lucida Sans", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRetroceder.Image = ((System.Drawing.Image)(resources.GetObject("btnRetroceder.Image")));
            this.btnRetroceder.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.btnRetroceder.Location = new System.Drawing.Point(0, 0);
            this.btnRetroceder.Name = "btnRetroceder";
            this.btnRetroceder.Size = new System.Drawing.Size(69, 82);
            this.btnRetroceder.TabIndex = 49;
            this.btnRetroceder.UseVisualStyleBackColor = true;
            this.btnRetroceder.Click += new System.EventHandler(this.btnRetroceder_Click);
            // 
            // btnEfectivo
            // 
            this.btnEfectivo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEfectivo.BackColor = System.Drawing.Color.Gray;
            this.btnEfectivo.FlatAppearance.BorderSize = 2;
            this.btnEfectivo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnEfectivo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnEfectivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEfectivo.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEfectivo.ForeColor = System.Drawing.Color.Black;
            this.btnEfectivo.Image = ((System.Drawing.Image)(resources.GetObject("btnEfectivo.Image")));
            this.btnEfectivo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEfectivo.Location = new System.Drawing.Point(199, 9);
            this.btnEfectivo.Name = "btnEfectivo";
            this.btnEfectivo.Size = new System.Drawing.Size(157, 67);
            this.btnEfectivo.TabIndex = 50;
            this.btnEfectivo.Text = "        Pagos en Efectivo";
            this.btnEfectivo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEfectivo.UseVisualStyleBackColor = false;
            this.btnEfectivo.Click += new System.EventHandler(this.btnEfectivo_Click);
            // 
            // btnTarjeta
            // 
            this.btnTarjeta.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnTarjeta.BackColor = System.Drawing.Color.Gray;
            this.btnTarjeta.FlatAppearance.BorderSize = 2;
            this.btnTarjeta.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnTarjeta.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnTarjeta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTarjeta.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTarjeta.ForeColor = System.Drawing.Color.Black;
            this.btnTarjeta.Image = ((System.Drawing.Image)(resources.GetObject("btnTarjeta.Image")));
            this.btnTarjeta.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTarjeta.Location = new System.Drawing.Point(451, 9);
            this.btnTarjeta.Name = "btnTarjeta";
            this.btnTarjeta.Size = new System.Drawing.Size(148, 67);
            this.btnTarjeta.TabIndex = 51;
            this.btnTarjeta.Text = "              Pagos con tarjeta";
            this.btnTarjeta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTarjeta.UseVisualStyleBackColor = false;
            this.btnTarjeta.Click += new System.EventHandler(this.btnTarjeta_Click);
            // 
            // FrmVentas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmVentas";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FrmVentas";
            this.Load += new System.EventHandler(this.FrmVentas_Load);
            this.panel1.ResumeLayout(false);
            this.pnlDgVentas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridVentas)).EndInit();
            this.pnlMetodos.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlDgVentas;
        private System.Windows.Forms.Panel pnlMetodos;
        private System.Windows.Forms.Button btnRetroceder;
        private System.Windows.Forms.DataGridView dataGridVentas;
        private System.Windows.Forms.Button btnEfectivo;
        private System.Windows.Forms.Button btnTarjeta;
    }
}