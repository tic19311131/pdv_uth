﻿using Lib_pdv_uth_v1;
using System.Collections.Generic;

namespace pdv_uth_v1
{
    partial class FrmCatalogoProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void mostrarRegistrosEnDG()
        {
            //borrar todos los ren del DG
            if (dgProductos.Rows.Count < 0)
                dgProductos.Rows.Clear();


            //se crea lista de criterios de busqueda
            List<CriteriosBusqueda> criterios = new List<CriteriosBusqueda>();
            //se crea objeto de crioterio para busqueda
            CriteriosBusqueda criterio = new CriteriosBusqueda();
            //se asignan los datos del criterio del WHERE
            criterio.campo = " 1 ";
            criterio.operadorIntermedio = OperadorDeConsulta.IGUAL;
            criterio.valor = "1";
            criterio.operadorFinal = OperadorDeConsulta.NINGUNO;
            //se incluye el criterio en la lista de criterios
            criterios.Add(criterio);
            //se ejecuta la consulta y se asigna el resultado al DtaGrid
            dgProductos.DataSource = prod.consultar(criterios);
            //se refresca el dataGrid para mostrar los datos
            dgProductos.Refresh();

        }
        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmCatalogoProductos));
            this.panelFormulario = new System.Windows.Forms.Panel();
            this.paneltotal = new System.Windows.Forms.Panel();
            this.panelIzquierdo = new System.Windows.Forms.Panel();
            this.btnAgregarProducto = new System.Windows.Forms.Button();
            this.checkBoxEsPereceredo = new System.Windows.Forms.CheckBox();
            this.comboUnidadDeMedida = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPrecio = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodBarras = new System.Windows.Forms.TextBox();
            this.lblCodBarras = new System.Windows.Forms.Label();
            this.txtDescripcion = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.panelImagen = new System.Windows.Forms.Panel();
            this.lblImagenOriginal = new System.Windows.Forms.Label();
            this.lblImagenAGuardar = new System.Windows.Forms.Label();
            this.icoBtnCargarImagen = new System.Windows.Forms.Button();
            this.picBoxImagenProducto = new System.Windows.Forms.PictureBox();
            this.panelbotones = new System.Windows.Forms.Panel();
            this.btnEliminarProducto = new System.Windows.Forms.Button();
            this.btnGuardarProducto = new System.Windows.Forms.Button();
            this.btnModificarProducto = new System.Windows.Forms.Button();
            this.pnlBarraTareas = new System.Windows.Forms.Panel();
            this.btnApagar = new System.Windows.Forms.Button();
            this.PanelData = new System.Windows.Forms.Panel();
            this.dgProductos = new System.Windows.Forms.DataGridView();
            this.pnlNavbar = new System.Windows.Forms.Panel();
            this.btnCajas = new System.Windows.Forms.Button();
            this.btnVentas = new System.Windows.Forms.Button();
            this.btnProductos = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.PnlInfo = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.lblCajaTitle = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ipbLogo = new System.Windows.Forms.PictureBox();
            this.panelTimer = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblReloj = new System.Windows.Forms.Label();
            this.openFileDialogCargarImagen = new System.Windows.Forms.OpenFileDialog();
            this.panelFormulario.SuspendLayout();
            this.paneltotal.SuspendLayout();
            this.panelIzquierdo.SuspendLayout();
            this.panelImagen.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxImagenProducto)).BeginInit();
            this.panelbotones.SuspendLayout();
            this.pnlBarraTareas.SuspendLayout();
            this.PanelData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).BeginInit();
            this.pnlNavbar.SuspendLayout();
            this.PnlInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipbLogo)).BeginInit();
            this.panelTimer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelFormulario
            // 
            this.panelFormulario.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panelFormulario.Controls.Add(this.paneltotal);
            this.panelFormulario.Controls.Add(this.pnlBarraTareas);
            this.panelFormulario.Controls.Add(this.PanelData);
            this.panelFormulario.Controls.Add(this.pnlNavbar);
            this.panelFormulario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFormulario.Location = new System.Drawing.Point(0, 0);
            this.panelFormulario.Margin = new System.Windows.Forms.Padding(2);
            this.panelFormulario.Name = "panelFormulario";
            this.panelFormulario.Size = new System.Drawing.Size(1208, 665);
            this.panelFormulario.TabIndex = 2;
            // 
            // paneltotal
            // 
            this.paneltotal.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.paneltotal.Controls.Add(this.panelIzquierdo);
            this.paneltotal.Controls.Add(this.panelImagen);
            this.paneltotal.Controls.Add(this.panelbotones);
            this.paneltotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.paneltotal.Location = new System.Drawing.Point(269, 67);
            this.paneltotal.Name = "paneltotal";
            this.paneltotal.Size = new System.Drawing.Size(939, 451);
            this.paneltotal.TabIndex = 23;
            // 
            // panelIzquierdo
            // 
            this.panelIzquierdo.Controls.Add(this.btnAgregarProducto);
            this.panelIzquierdo.Controls.Add(this.checkBoxEsPereceredo);
            this.panelIzquierdo.Controls.Add(this.comboUnidadDeMedida);
            this.panelIzquierdo.Controls.Add(this.label4);
            this.panelIzquierdo.Controls.Add(this.txtPrecio);
            this.panelIzquierdo.Controls.Add(this.label2);
            this.panelIzquierdo.Controls.Add(this.txtCodBarras);
            this.panelIzquierdo.Controls.Add(this.lblCodBarras);
            this.panelIzquierdo.Controls.Add(this.txtDescripcion);
            this.panelIzquierdo.Controls.Add(this.label1);
            this.panelIzquierdo.Controls.Add(this.lblNombre);
            this.panelIzquierdo.Controls.Add(this.txtNombre);
            this.panelIzquierdo.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelIzquierdo.Location = new System.Drawing.Point(0, 0);
            this.panelIzquierdo.Name = "panelIzquierdo";
            this.panelIzquierdo.Size = new System.Drawing.Size(375, 451);
            this.panelIzquierdo.TabIndex = 46;
            // 
            // btnAgregarProducto
            // 
            this.btnAgregarProducto.FlatAppearance.BorderSize = 2;
            this.btnAgregarProducto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnAgregarProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnAgregarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregarProducto.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregarProducto.ForeColor = System.Drawing.Color.Black;
            this.btnAgregarProducto.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregarProducto.Image")));
            this.btnAgregarProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnAgregarProducto.Location = new System.Drawing.Point(13, 3);
            this.btnAgregarProducto.Name = "btnAgregarProducto";
            this.btnAgregarProducto.Size = new System.Drawing.Size(128, 57);
            this.btnAgregarProducto.TabIndex = 48;
            this.btnAgregarProducto.Text = "Agregar Producto";
            this.btnAgregarProducto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAgregarProducto.UseVisualStyleBackColor = true;
            this.btnAgregarProducto.Click += new System.EventHandler(this.btnAgregarProducto_Click);
            // 
            // checkBoxEsPereceredo
            // 
            this.checkBoxEsPereceredo.AutoSize = true;
            this.checkBoxEsPereceredo.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxEsPereceredo.ForeColor = System.Drawing.Color.Black;
            this.checkBoxEsPereceredo.Location = new System.Drawing.Point(202, 358);
            this.checkBoxEsPereceredo.Margin = new System.Windows.Forms.Padding(2);
            this.checkBoxEsPereceredo.Name = "checkBoxEsPereceredo";
            this.checkBoxEsPereceredo.Size = new System.Drawing.Size(132, 19);
            this.checkBoxEsPereceredo.TabIndex = 40;
            this.checkBoxEsPereceredo.Text = "¿Es Perecedero?";
            this.checkBoxEsPereceredo.UseVisualStyleBackColor = true;
            // 
            // comboUnidadDeMedida
            // 
            this.comboUnidadDeMedida.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.comboUnidadDeMedida.FormattingEnabled = true;
            this.comboUnidadDeMedida.Items.AddRange(new object[] {
            "UNIDAD",
            "LITRO",
            "KILO",
            "CAJA",
            "PAQUETE"});
            this.comboUnidadDeMedida.Location = new System.Drawing.Point(202, 245);
            this.comboUnidadDeMedida.Margin = new System.Windows.Forms.Padding(2);
            this.comboUnidadDeMedida.Name = "comboUnidadDeMedida";
            this.comboUnidadDeMedida.Size = new System.Drawing.Size(150, 21);
            this.comboUnidadDeMedida.TabIndex = 39;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.label4.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(214, 221);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 15);
            this.label4.TabIndex = 38;
            this.label4.Text = "Unidad de Medida";
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(25, 357);
            this.txtPrecio.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(150, 20);
            this.txtPrecio.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.label2.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(75, 329);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 15);
            this.label2.TabIndex = 36;
            this.label2.Text = "Precio";
            // 
            // txtCodBarras
            // 
            this.txtCodBarras.Location = new System.Drawing.Point(16, 246);
            this.txtCodBarras.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodBarras.Name = "txtCodBarras";
            this.txtCodBarras.Size = new System.Drawing.Size(159, 20);
            this.txtCodBarras.TabIndex = 35;
            this.txtCodBarras.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodBarras_KeyPress);
            // 
            // lblCodBarras
            // 
            this.lblCodBarras.AutoSize = true;
            this.lblCodBarras.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.lblCodBarras.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodBarras.ForeColor = System.Drawing.Color.Black;
            this.lblCodBarras.Location = new System.Drawing.Point(41, 221);
            this.lblCodBarras.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblCodBarras.Name = "lblCodBarras";
            this.lblCodBarras.Size = new System.Drawing.Size(83, 15);
            this.lblCodBarras.TabIndex = 34;
            this.lblCodBarras.Text = "Cod. Barras";
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(13, 148);
            this.txtDescripcion.Margin = new System.Windows.Forms.Padding(2);
            this.txtDescripcion.Multiline = true;
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(321, 63);
            this.txtDescripcion.TabIndex = 33;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.label1.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(13, 122);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 15);
            this.label1.TabIndex = 32;
            this.label1.Text = "Descripción";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.lblNombre.Font = new System.Drawing.Font("Lucida Bright", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.ForeColor = System.Drawing.Color.Black;
            this.lblNombre.Location = new System.Drawing.Point(13, 77);
            this.lblNombre.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(59, 15);
            this.lblNombre.TabIndex = 30;
            this.lblNombre.Text = "Nombre";
            // 
            // txtNombre
            // 
            this.txtNombre.Location = new System.Drawing.Point(16, 100);
            this.txtNombre.Margin = new System.Windows.Forms.Padding(2);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(200, 20);
            this.txtNombre.TabIndex = 31;
            // 
            // panelImagen
            // 
            this.panelImagen.Controls.Add(this.lblImagenOriginal);
            this.panelImagen.Controls.Add(this.lblImagenAGuardar);
            this.panelImagen.Controls.Add(this.icoBtnCargarImagen);
            this.panelImagen.Controls.Add(this.picBoxImagenProducto);
            this.panelImagen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelImagen.Location = new System.Drawing.Point(0, 0);
            this.panelImagen.Name = "panelImagen";
            this.panelImagen.Size = new System.Drawing.Size(776, 451);
            this.panelImagen.TabIndex = 45;
            // 
            // lblImagenOriginal
            // 
            this.lblImagenOriginal.AutoSize = true;
            this.lblImagenOriginal.Font = new System.Drawing.Font("Lucida Bright", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenOriginal.ForeColor = System.Drawing.Color.Black;
            this.lblImagenOriginal.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lblImagenOriginal.Location = new System.Drawing.Point(494, 357);
            this.lblImagenOriginal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenOriginal.Name = "lblImagenOriginal";
            this.lblImagenOriginal.Size = new System.Drawing.Size(133, 17);
            this.lblImagenOriginal.TabIndex = 26;
            this.lblImagenOriginal.Text = "Nombre Original";
            this.lblImagenOriginal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblImagenOriginal.Visible = false;
            // 
            // lblImagenAGuardar
            // 
            this.lblImagenAGuardar.AutoSize = true;
            this.lblImagenAGuardar.Font = new System.Drawing.Font("Lucida Bright", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImagenAGuardar.ForeColor = System.Drawing.Color.Black;
            this.lblImagenAGuardar.Location = new System.Drawing.Point(485, 392);
            this.lblImagenAGuardar.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblImagenAGuardar.Name = "lblImagenAGuardar";
            this.lblImagenAGuardar.Size = new System.Drawing.Size(151, 17);
            this.lblImagenAGuardar.TabIndex = 27;
            this.lblImagenAGuardar.Text = "Nombre a Guardar\r\n";
            this.lblImagenAGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // icoBtnCargarImagen
            // 
            this.icoBtnCargarImagen.BackColor = System.Drawing.Color.Gray;
            this.icoBtnCargarImagen.Font = new System.Drawing.Font("Lucida Bright", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.icoBtnCargarImagen.Image = ((System.Drawing.Image)(resources.GetObject("icoBtnCargarImagen.Image")));
            this.icoBtnCargarImagen.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.icoBtnCargarImagen.Location = new System.Drawing.Point(458, 431);
            this.icoBtnCargarImagen.Name = "icoBtnCargarImagen";
            this.icoBtnCargarImagen.Size = new System.Drawing.Size(252, 60);
            this.icoBtnCargarImagen.TabIndex = 24;
            this.icoBtnCargarImagen.Text = "      Cargar Imagen";
            this.icoBtnCargarImagen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.icoBtnCargarImagen.UseVisualStyleBackColor = false;
            this.icoBtnCargarImagen.Click += new System.EventHandler(this.icoBtnCargarImagen_Click);
            // 
            // picBoxImagenProducto
            // 
            this.picBoxImagenProducto.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.picBoxImagenProducto.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.picBoxImagenProducto.Enabled = false;
            this.picBoxImagenProducto.Location = new System.Drawing.Point(380, 53);
            this.picBoxImagenProducto.Margin = new System.Windows.Forms.Padding(2);
            this.picBoxImagenProducto.Name = "picBoxImagenProducto";
            this.picBoxImagenProducto.Size = new System.Drawing.Size(371, 291);
            this.picBoxImagenProducto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picBoxImagenProducto.TabIndex = 18;
            this.picBoxImagenProducto.TabStop = false;
            // 
            // panelbotones
            // 
            this.panelbotones.Controls.Add(this.btnEliminarProducto);
            this.panelbotones.Controls.Add(this.btnGuardarProducto);
            this.panelbotones.Controls.Add(this.btnModificarProducto);
            this.panelbotones.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelbotones.Location = new System.Drawing.Point(776, 0);
            this.panelbotones.Name = "panelbotones";
            this.panelbotones.Size = new System.Drawing.Size(163, 451);
            this.panelbotones.TabIndex = 44;
            // 
            // btnEliminarProducto
            // 
            this.btnEliminarProducto.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnEliminarProducto.BackColor = System.Drawing.Color.Gray;
            this.btnEliminarProducto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEliminarProducto.FlatAppearance.BorderSize = 2;
            this.btnEliminarProducto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnEliminarProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnEliminarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEliminarProducto.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminarProducto.ForeColor = System.Drawing.Color.Black;
            this.btnEliminarProducto.Image = ((System.Drawing.Image)(resources.GetObject("btnEliminarProducto.Image")));
            this.btnEliminarProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminarProducto.Location = new System.Drawing.Point(7, 351);
            this.btnEliminarProducto.Name = "btnEliminarProducto";
            this.btnEliminarProducto.Size = new System.Drawing.Size(137, 57);
            this.btnEliminarProducto.TabIndex = 41;
            this.btnEliminarProducto.Text = "             Eliminar     Producto";
            this.btnEliminarProducto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminarProducto.UseVisualStyleBackColor = false;
            this.btnEliminarProducto.Click += new System.EventHandler(this.btnEliminarProducto_Click);
            // 
            // btnGuardarProducto
            // 
            this.btnGuardarProducto.BackColor = System.Drawing.Color.Gray;
            this.btnGuardarProducto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGuardarProducto.FlatAppearance.BorderSize = 2;
            this.btnGuardarProducto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnGuardarProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnGuardarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardarProducto.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarProducto.ForeColor = System.Drawing.Color.Black;
            this.btnGuardarProducto.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardarProducto.Image")));
            this.btnGuardarProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnGuardarProducto.Location = new System.Drawing.Point(8, 220);
            this.btnGuardarProducto.Name = "btnGuardarProducto";
            this.btnGuardarProducto.Size = new System.Drawing.Size(136, 57);
            this.btnGuardarProducto.TabIndex = 42;
            this.btnGuardarProducto.Text = "Guardar Producto";
            this.btnGuardarProducto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnGuardarProducto.UseVisualStyleBackColor = false;
            this.btnGuardarProducto.Click += new System.EventHandler(this.btnGuardarProducto_Click_1);
            // 
            // btnModificarProducto
            // 
            this.btnModificarProducto.BackColor = System.Drawing.Color.Gray;
            this.btnModificarProducto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnModificarProducto.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnModificarProducto.FlatAppearance.BorderSize = 2;
            this.btnModificarProducto.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnModificarProducto.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnModificarProducto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnModificarProducto.Font = new System.Drawing.Font("Lucida Bright", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificarProducto.ForeColor = System.Drawing.Color.Black;
            this.btnModificarProducto.Image = ((System.Drawing.Image)(resources.GetObject("btnModificarProducto.Image")));
            this.btnModificarProducto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificarProducto.Location = new System.Drawing.Point(6, 72);
            this.btnModificarProducto.Name = "btnModificarProducto";
            this.btnModificarProducto.Size = new System.Drawing.Size(137, 65);
            this.btnModificarProducto.TabIndex = 43;
            this.btnModificarProducto.Text = "                                      Modificar            Producto";
            this.btnModificarProducto.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificarProducto.UseVisualStyleBackColor = false;
            this.btnModificarProducto.Click += new System.EventHandler(this.btnModificarProducto_Click);
            // 
            // pnlBarraTareas
            // 
            this.pnlBarraTareas.BackColor = System.Drawing.Color.Gray;
            this.pnlBarraTareas.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlBarraTareas.Controls.Add(this.btnApagar);
            this.pnlBarraTareas.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlBarraTareas.Location = new System.Drawing.Point(269, 0);
            this.pnlBarraTareas.Name = "pnlBarraTareas";
            this.pnlBarraTareas.Size = new System.Drawing.Size(939, 67);
            this.pnlBarraTareas.TabIndex = 21;
            // 
            // btnApagar
            // 
            this.btnApagar.BackColor = System.Drawing.Color.Gray;
            this.btnApagar.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnApagar.FlatAppearance.BorderSize = 0;
            this.btnApagar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnApagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnApagar.Font = new System.Drawing.Font("Lucida Bright", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnApagar.ForeColor = System.Drawing.Color.Black;
            this.btnApagar.Image = ((System.Drawing.Image)(resources.GetObject("btnApagar.Image")));
            this.btnApagar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnApagar.Location = new System.Drawing.Point(882, 0);
            this.btnApagar.Name = "btnApagar";
            this.btnApagar.Size = new System.Drawing.Size(53, 63);
            this.btnApagar.TabIndex = 46;
            this.btnApagar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnApagar.UseVisualStyleBackColor = false;
            this.btnApagar.Click += new System.EventHandler(this.btnApagar_Click);
            // 
            // PanelData
            // 
            this.PanelData.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.PanelData.Controls.Add(this.dgProductos);
            this.PanelData.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelData.Location = new System.Drawing.Point(269, 518);
            this.PanelData.Name = "PanelData";
            this.PanelData.Size = new System.Drawing.Size(939, 147);
            this.PanelData.TabIndex = 20;
            // 
            // dgProductos
            // 
            this.dgProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProductos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgProductos.Location = new System.Drawing.Point(0, 0);
            this.dgProductos.Name = "dgProductos";
            this.dgProductos.Size = new System.Drawing.Size(939, 147);
            this.dgProductos.TabIndex = 0;
            this.dgProductos.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProductos_CellContentClick);
            this.dgProductos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodBarras_KeyPress);
            // 
            // pnlNavbar
            // 
            this.pnlNavbar.BackColor = System.Drawing.Color.Gray;
            this.pnlNavbar.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlNavbar.Controls.Add(this.btnCajas);
            this.pnlNavbar.Controls.Add(this.btnVentas);
            this.pnlNavbar.Controls.Add(this.btnProductos);
            this.pnlNavbar.Controls.Add(this.btnHome);
            this.pnlNavbar.Controls.Add(this.PnlInfo);
            this.pnlNavbar.Controls.Add(this.panelTimer);
            this.pnlNavbar.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlNavbar.Location = new System.Drawing.Point(0, 0);
            this.pnlNavbar.Name = "pnlNavbar";
            this.pnlNavbar.Size = new System.Drawing.Size(269, 665);
            this.pnlNavbar.TabIndex = 20;
            // 
            // btnCajas
            // 
            this.btnCajas.BackColor = System.Drawing.Color.Gray;
            this.btnCajas.FlatAppearance.BorderSize = 2;
            this.btnCajas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnCajas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnCajas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCajas.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCajas.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCajas.Image = ((System.Drawing.Image)(resources.GetObject("btnCajas.Image")));
            this.btnCajas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCajas.Location = new System.Drawing.Point(5, 492);
            this.btnCajas.Name = "btnCajas";
            this.btnCajas.Size = new System.Drawing.Size(249, 62);
            this.btnCajas.TabIndex = 52;
            this.btnCajas.Text = "   Cajas";
            this.btnCajas.UseVisualStyleBackColor = false;
            this.btnCajas.Click += new System.EventHandler(this.btnCajas_Click);
            // 
            // btnVentas
            // 
            this.btnVentas.BackColor = System.Drawing.Color.Gray;
            this.btnVentas.FlatAppearance.BorderSize = 2;
            this.btnVentas.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnVentas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnVentas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVentas.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVentas.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnVentas.Image = ((System.Drawing.Image)(resources.GetObject("btnVentas.Image")));
            this.btnVentas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVentas.Location = new System.Drawing.Point(5, 341);
            this.btnVentas.Name = "btnVentas";
            this.btnVentas.Size = new System.Drawing.Size(249, 64);
            this.btnVentas.TabIndex = 51;
            this.btnVentas.Text = "    Ventas";
            this.btnVentas.UseVisualStyleBackColor = false;
            this.btnVentas.Click += new System.EventHandler(this.btnVentas_Click);
            // 
            // btnProductos
            // 
            this.btnProductos.BackColor = System.Drawing.Color.Gray;
            this.btnProductos.FlatAppearance.BorderSize = 2;
            this.btnProductos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnProductos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnProductos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductos.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProductos.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnProductos.Image = ((System.Drawing.Image)(resources.GetObject("btnProductos.Image")));
            this.btnProductos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProductos.Location = new System.Drawing.Point(5, 415);
            this.btnProductos.Name = "btnProductos";
            this.btnProductos.Size = new System.Drawing.Size(249, 65);
            this.btnProductos.TabIndex = 50;
            this.btnProductos.Text = "     Productos";
            this.btnProductos.UseVisualStyleBackColor = false;
            this.btnProductos.Click += new System.EventHandler(this.btnProductos_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.Gray;
            this.btnHome.FlatAppearance.BorderSize = 2;
            this.btnHome.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnHome.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnHome.Image = ((System.Drawing.Image)(resources.GetObject("btnHome.Image")));
            this.btnHome.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnHome.Location = new System.Drawing.Point(5, 265);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(249, 60);
            this.btnHome.TabIndex = 49;
            this.btnHome.Text = "     Inicio";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // PnlInfo
            // 
            this.PnlInfo.BackColor = System.Drawing.Color.Gray;
            this.PnlInfo.Controls.Add(this.panel3);
            this.PnlInfo.Controls.Add(this.pictureBox14);
            this.PnlInfo.Controls.Add(this.lblCajaTitle);
            this.PnlInfo.Controls.Add(this.label15);
            this.PnlInfo.Controls.Add(this.ipbLogo);
            this.PnlInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PnlInfo.Location = new System.Drawing.Point(0, 0);
            this.PnlInfo.Name = "PnlInfo";
            this.PnlInfo.Size = new System.Drawing.Size(265, 259);
            this.PnlInfo.TabIndex = 41;
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(268, 4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(600, 42);
            this.panel3.TabIndex = 48;
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackColor = System.Drawing.Color.Gray;
            this.pictureBox14.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox14.Image")));
            this.pictureBox14.Location = new System.Drawing.Point(10, 1);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(54, 58);
            this.pictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox14.TabIndex = 47;
            this.pictureBox14.TabStop = false;
            // 
            // lblCajaTitle
            // 
            this.lblCajaTitle.AutoSize = true;
            this.lblCajaTitle.BackColor = System.Drawing.Color.Gray;
            this.lblCajaTitle.Font = new System.Drawing.Font("Lucida Bright", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCajaTitle.ForeColor = System.Drawing.Color.Black;
            this.lblCajaTitle.Location = new System.Drawing.Point(93, 12);
            this.lblCajaTitle.Name = "lblCajaTitle";
            this.lblCajaTitle.Size = new System.Drawing.Size(161, 33);
            this.lblCajaTitle.TabIndex = 46;
            this.lblCajaTitle.Text = "Productos";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Gray;
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(68, 236);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(133, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "NOMBRE DEL USUARIO ";
            this.label15.UseWaitCursor = true;
            // 
            // ipbLogo
            // 
            this.ipbLogo.BackColor = System.Drawing.Color.Gray;
            this.ipbLogo.Image = ((System.Drawing.Image)(resources.GetObject("ipbLogo.Image")));
            this.ipbLogo.Location = new System.Drawing.Point(37, 65);
            this.ipbLogo.Name = "ipbLogo";
            this.ipbLogo.Size = new System.Drawing.Size(175, 168);
            this.ipbLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ipbLogo.TabIndex = 43;
            this.ipbLogo.TabStop = false;
            this.ipbLogo.UseWaitCursor = true;
            // 
            // panelTimer
            // 
            this.panelTimer.BackColor = System.Drawing.Color.Gray;
            this.panelTimer.Controls.Add(this.pictureBox1);
            this.panelTimer.Controls.Add(this.lblReloj);
            this.panelTimer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelTimer.Location = new System.Drawing.Point(0, 560);
            this.panelTimer.Name = "panelTimer";
            this.panelTimer.Size = new System.Drawing.Size(265, 101);
            this.panelTimer.TabIndex = 43;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(10, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(54, 54);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // lblReloj
            // 
            this.lblReloj.BackColor = System.Drawing.Color.Gray;
            this.lblReloj.Font = new System.Drawing.Font("Lucida Bright", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReloj.ForeColor = System.Drawing.Color.Black;
            this.lblReloj.Location = new System.Drawing.Point(70, 38);
            this.lblReloj.Name = "lblReloj";
            this.lblReloj.Size = new System.Drawing.Size(164, 32);
            this.lblReloj.TabIndex = 0;
            // 
            // openFileDialogCargarImagen
            // 
            this.openFileDialogCargarImagen.FileName = "openFileDialog1";
            // 
            // FrmCatalogoProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1208, 665);
            this.Controls.Add(this.panelFormulario);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmCatalogoProductos";
            this.Text = "FrmCatalogoProductos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmCatalogoProductos_Load);
            this.panelFormulario.ResumeLayout(false);
            this.paneltotal.ResumeLayout(false);
            this.panelIzquierdo.ResumeLayout(false);
            this.panelIzquierdo.PerformLayout();
            this.panelImagen.ResumeLayout(false);
            this.panelImagen.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxImagenProducto)).EndInit();
            this.panelbotones.ResumeLayout(false);
            this.pnlBarraTareas.ResumeLayout(false);
            this.PanelData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgProductos)).EndInit();
            this.pnlNavbar.ResumeLayout(false);
            this.PnlInfo.ResumeLayout(false);
            this.PnlInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ipbLogo)).EndInit();
            this.panelTimer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelFormulario;
        private System.Windows.Forms.OpenFileDialog openFileDialogCargarImagen;
        private System.Windows.Forms.Panel pnlNavbar;
        private System.Windows.Forms.Panel panelTimer;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblReloj;
        private System.Windows.Forms.Panel PnlInfo;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.Label lblCajaTitle;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox ipbLogo;
        private System.Windows.Forms.Panel pnlBarraTareas;
        private System.Windows.Forms.Button btnApagar;
        private System.Windows.Forms.Panel PanelData;
        private System.Windows.Forms.DataGridView dgProductos;
        private System.Windows.Forms.Button btnAgregarProducto;
        private System.Windows.Forms.Panel paneltotal;
        private System.Windows.Forms.Panel panelIzquierdo;
        private System.Windows.Forms.CheckBox checkBoxEsPereceredo;
        private System.Windows.Forms.ComboBox comboUnidadDeMedida;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPrecio;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtCodBarras;
        private System.Windows.Forms.Label lblCodBarras;
        private System.Windows.Forms.TextBox txtDescripcion;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Panel panelImagen;
        private System.Windows.Forms.Button icoBtnCargarImagen;
        private System.Windows.Forms.PictureBox picBoxImagenProducto;
        private System.Windows.Forms.Panel panelbotones;
        private System.Windows.Forms.Button btnEliminarProducto;
        private System.Windows.Forms.Button btnGuardarProducto;
        private System.Windows.Forms.Button btnModificarProducto;
        private System.Windows.Forms.Label lblImagenOriginal;
        private System.Windows.Forms.Label lblImagenAGuardar;
        private System.Windows.Forms.Button btnCajas;
        private System.Windows.Forms.Button btnVentas;
        private System.Windows.Forms.Button btnProductos;
        private System.Windows.Forms.Button btnHome;
    }
}