﻿using LibBD;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib_pdv_uth_v1.Ventas
{
    public class VentasTarjeta
    {
        LibMySql bd;
        public VentasTarjeta()
        {
            bd = new LibMySql("127.0.0.1", "root", "negro9720", "pdv_uth_bd_v1");
        }

        int id;
        int venta_id;
        string vigencia_mes;
        string vigencia_anio;
        string numero_autorizacion;
        DateTime fecha_hora;
        double monto;

        public int Id { get => id; set => id = value; }
        public int Venta_id { get => venta_id; set => venta_id = value; }
        public string Vigencia_mes { get => vigencia_mes; set => vigencia_mes = value; }
        public string Vigencia_anio { get => vigencia_anio; set => vigencia_anio = value; }
        public string Numero_autorizacion { get => numero_autorizacion; set => numero_autorizacion = value; }
        public DateTime Fecha_hora { get => fecha_hora; set => fecha_hora = value; }
        public double Monto { get => monto; set => monto = value; }

        public List<object> consultar(List<CriteriosBusqueda> criteriosBusqueda)
        {
            //lista de object para resultado
            List<object> res = new List<object>();
            //hacemos el where
            string where = "";// "folio LIKE '%" + folio + "%'";
            //interpretamos los operadores de cada condiciones del WHERE
            for (int i = 0; i < criteriosBusqueda.Count; i++)
                where += " " + criteriosBusqueda[i].campo + " " + criteriosBusqueda[i].opIntermedioSql() + " " + criteriosBusqueda[i].valor + " " + criteriosBusqueda[i].opFinalSql() + " ";
            //hacemos la consulta
            List<object> tmp = bd.consultar("*", "ventas_con_tarjetas", where);
            //mapeamos cada Object en un Cliente
            foreach (object[] prodTmp in tmp)
            {
                var prod = new
                {
                    Id = int.Parse(prodTmp[0].ToString()),
                    Venta_id = (prodTmp[1].ToString()),
                    Vigencia_mes = (prodTmp[2].ToString()),
                    Vigencia_anio =(prodTmp[3].ToString()),
                    Numero_autorizacion = (prodTmp[4].ToString()),
                    Fecha_hora = DateTime.Parse(prodTmp[5].ToString()),
                    Monto = double.Parse(prodTmp[6].ToString()),

                };
                //se agrega este prod a la lista e productos
                res.Add(prod);
            }
            //regresamos la lista de cliente
            return res;
        }


    }
}
