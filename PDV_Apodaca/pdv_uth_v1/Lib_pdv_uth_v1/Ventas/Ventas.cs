﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.Cryptography;
using System.Text;
using Lib_pdv_uth_v1.usuarios;
using LibBD;

namespace Lib_pdv_uth_v1.Ventas
{

    public class Ventas
    {

        LibMySql bd;
        private List<Ventas> tablaVentas = new List<Ventas>();

        int id;
        int folio;
        int caja_id;
        int usuario;
        DateTime fecha_hora;
        double total_venta;

        public int Id { get => id; set => id = value; }
        public int Folio { get => folio; set => folio = value; }
        public int Caja_id { get => caja_id; set => caja_id = value; }
        public int Usuario { get => usuario; set => usuario = value; }

        public double Total_venta { get => double.Parse(total_venta.ToString("F2", CultureInfo.CreateSpecificCulture("en-US"))); set => this.total_venta = double.Parse(value.ToString("F2", CultureInfo.CreateSpecificCulture("en-US"))); }
        public List<Ventas> TablaVentas { get => tablaVentas; set => tablaVentas = value; }
        public DateTime Fecha_hora { get => fecha_hora; set => fecha_hora = value; }

        public Ventas()
        {
            bd = new LibMySql("127.0.0.1", "root", "negro9720", "pdv_uth_bd_v1");
        }


        public List<object> consultar(List<CriteriosBusqueda> criteriosBusqueda)
        {
            //lista de object para resultado
            List<object> res = new List<object>();
            //hacemos el where
            string where = "";// "folio LIKE '%" + folio + "%'";
            //interpretamos los operadores de cada condiciones del WHERE
            for (int i = 0; i < criteriosBusqueda.Count; i++)
                where += " " + criteriosBusqueda[i].campo + " " + criteriosBusqueda[i].opIntermedioSql() + " " + criteriosBusqueda[i].valor + " " + criteriosBusqueda[i].opFinalSql() + " ";
            //hacemos la consulta
            List<object> tmp = bd.consultar("*", "ventas", where);
            //mapeamos cada Object en un Cliente
            foreach (object[] prodTmp in tmp)
            {
                var prod = new
                {
                    Id = int.Parse(prodTmp[0].ToString()),
                    Folio = int.Parse(prodTmp[1].ToString()),
                    Caja_id = int.Parse(prodTmp[2].ToString()),
                    Usuario = int.Parse(prodTmp[3].ToString()),
                    Fecha_hora = DateTime.Parse(prodTmp[4].ToString()),
                    Total_venta = double.Parse(prodTmp[5].ToString()),

                };
                //se agrega este prod a la lista e productos
                res.Add(prod);
            }
            //regresamos la lista de cliente
            return res;
            // //lista de object para resultado
            // List<object> res = new List<object>();
            // //hacemos el where
            // string where = "folio LIKE '%" + folio + "%'";
            // //interpretamos los operadores de cada condiciones del WHERE
            // for (int i = 0; i < criteriosBusqueda.Count; i++)
            //     where += " " + criteriosBusqueda[i].campo + " " + criteriosBusqueda[i].opIntermedioSql() + " " + criteriosBusqueda[i].valor + " " + criteriosBusqueda[i].opFinalSql() + " ";
            // //hacemos la consulta
            // List<object> tmp = bd.consultar("*", "ventas", where);
            // //mapeamos cada Object en un Cliente
            // foreach (object[] prodTmp in tmp)
            // {
            //     var prod = new
            //     {
            //         Id = int.Parse(prodTmp[0].ToString()),
            //         Folio = int.Parse(prodTmp[1].ToString()),
            //         Caja_id = int.Parse(prodTmp[2].ToString()),
            //         Usuario = int.Parse(prodTmp[3].ToString()),
            //         Fecha_hora = int.Parse(prodTmp[4].ToString()),
            //         Total_venta = double.Parse(prodTmp[5].ToString()),
            //
            //     };
            //     //se agrega este prod a la lista e productos
            //     res.Add(prod);
            // }
            // //regresamos la lista de cliente
            // return res;

        
       
        }

    }
}
