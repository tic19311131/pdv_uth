﻿namespace Lib_pdv_uth_v1.productos
{
    public enum UnidadDeMedida
    {
        UNIDAD,
        LITRO,
        KILO,
        CAJA,
        PAQUETE
    }
}