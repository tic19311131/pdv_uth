
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LibBD;

namespace Lib_pdv_uth_v1.usuarios
{

    public class Usuario : Persona, ICrud
    {
        //Var de BD
        LibMySql bd;
        //var de errores
        public static string msgError = "";
        //constructor default
        public Usuario(): base(1, "", "",  new DateTime(), "", "","", "", new Domicilio(), "", "", "" )
        {
            bd = new LibMySql();
        }
        //props unicas de user
        private object actaNacimientoComprobante;
        private object cartaNoAntecedentesP;
        private object certificadoEscolar;
        private string numeroSeguroSocial;
        private TipoUsuario tipoUsuario;
        private string contraseña;

        //get y set
        public object ActaNacimientoComprobante { get => actaNacimientoComprobante; set => actaNacimientoComprobante = value; }
        public object CartaNoAntecedentesP { get => cartaNoAntecedentesP; set => cartaNoAntecedentesP = value; }
        public object CertificadoEscolar { get => certificadoEscolar; set => certificadoEscolar = value; }
        public string NumeroSeguroSocial { get => numeroSeguroSocial; set => numeroSeguroSocial = value; }
        public TipoUsuario TipoUsuario { get => tipoUsuario; set => tipoUsuario = value; }
        public string Contraseña { get => contraseña; set => contraseña = value; }

        public bool insertar(string nom, string apP, string apM, string cel, string telefono, string correo, string fechaNac,
                              string calle, string numCasa, string colonia, string fraccionamiento, string cp,
                              string localidad, string municipio, string comprobanteDom,
                              string comproINE, string curp,string comproCurp ,string actaNac, string certEstudio, string numSS,
                              string tipoUs, string pdw)
        {

            bool res = false;
            string valores = "'" + nom + "','" + apP + "','" + apM + "','" + cel + "','" + telefono + "','" + correo + "','" + fechaNac + "','" + calle + "','" + numCasa + "','" + cp + "','" + colonia + "','" + fraccionamiento + "','" + localidad + "','" + municipio + "','" 
                + comprobanteDom + "','" + comproINE + "','" + curp + "','" + comproCurp + "','" + actaNac + "','" + certEstudio +
                "','" + numSS + "','" + tipoUs + "','" + pdw + "'";
            if (bd.insertar("usuarios",
                            "nombre, apellido_paterno, apellido_materno,fecha_de_nacimiento, celular, telefono, correo," +
                            "calle, numero_casa, codigo_postal, colonia, fraccionamiento, localidad, municipio, img_comprobante_domicilio, ine_comprobante, " +
                            "curp, curp_comprobante, acta_nacimiento, certificado_estudios, numero_seguro_social, tipo_usuario, pwd ",
                            valores))
            {
                res = true;
            }
            else
            {
                msgError = "Error al dar de alta nuevo Usuario. " + LibMySql.msgError;
            }
            return res;
        }

        
        public bool alta( )
        {
            string fechaCorrecta = this.FechaNacimiento.Year + "/" + this.FechaNacimiento.Month + "/" + this.FechaNacimiento.Day;

            return insertar(this.Nombre, this.ApellidoPaterno, this.ApellidoMaterno, fechaCorrecta, this.Celular, this.telefono,
                            this.Correo, this.Domicilio.calle, this.Domicilio.numero,
                            this.Domicilio.colonia, this.Domicilio.codigoPostal, this.Domicilio.seccionFraccionamiento, this.Domicilio.localidad,
                            this.Domicilio.municipio, this.Domicilio.fotoComprobante.ToString(), this.ComprobanteINE.ToString(),
                            this.curp, this.curpCompro.ToString(), this.actaNacimientoComprobante.ToString(), this.certificadoEscolar.ToString(), this.numeroSeguroSocial.ToString(),
                            this.tipoUsuario.ToString(), this.contraseña);
        }
        
        public bool eliminar(int id)
        {
            return bd.eliminar("usuarios", " id=" + id);
        }
        public List<object> consultar(List<CriteriosBusqueda> criteriosBusqueda )
        {
            //lista de object para resultado
            List<object> res = new List<object>();
            //hacemos el where
            string where = "";
            //insttterpretamos los operadores de cada condiciones del WHERE
            for (int i = 0; i < criteriosBusqueda.Count; i++)
            {
                string opIntermedio = "";
                switch (criteriosBusqueda[i].operadorIntermedio)
                {
                    case OperadorDeConsulta.IGUAL: opIntermedio = "="; break;
                    case OperadorDeConsulta.LIKE: opIntermedio = "LIKE"; break;
                    case OperadorDeConsulta.DIFERENTE: opIntermedio = "<>"; break;
                    case OperadorDeConsulta.NO_IGUAL: opIntermedio = "!="; break;
                    default: opIntermedio = ""; break;
                }
                string opFinal = "";
                switch (criteriosBusqueda[i].operadorIntermedio)
                {
                    case OperadorDeConsulta.AND: opFinal = "AND"; break;
                    case OperadorDeConsulta.OR: opFinal = "OR"; break;

                    default: opFinal = ""; break;
                }
                where += " " + criteriosBusqueda[i].campo + " " + opIntermedio + " " + criteriosBusqueda[i].valor + " " + opFinal + " ";
            }//for para hacer where
            //hacemos la consulta
            List<object> tmp = bd.consultar("*", "usuarios", where);
            //mapeamos cada Object en un Cliente
            foreach (object[] cliTmp in tmp)
            {
                var cli = new
                {
                    Id = int.Parse(cliTmp[0].ToString()),
                    Nombre = cliTmp[1].ToString(),
                    ApellidoPaterno = cliTmp[2].ToString(),
                    ApellidoMaterno = cliTmp[3].ToString(),
                    FechaNacimiento = DateTime.Parse(cliTmp[4].ToString()),
                    Celular = cliTmp[5].ToString(),
                    Telefono = cliTmp[6].ToString(),
                    Correo = cliTmp[7].ToString(),
                    calle = cliTmp[8].ToString(),
                    numero = cliTmp[9].ToString(),
                    codigoPostal = cliTmp[10].ToString(),
                    colonia = cliTmp[11].ToString(),
                    seccionFraccionamiento = cliTmp[12].ToString(),
                    localidad = cliTmp[13].ToString(),
                    municipio = cliTmp[14].ToString(),
                    fotoComprobante = cliTmp[15].ToString(),
                    ComprobanteINE = cliTmp[16].ToString(),
                    Curp = cliTmp[17].ToString(),
                    CurpCompro = cliTmp[18].ToString(),
                    actaNacimientoComprobante = cliTmp[19].ToString(),
                    certificadoEscolar = cliTmp[20].ToString(),
                    numeroSeguroSocial = cliTmp[21].ToString(),
                    tipoUsuario = (TipoUsuario)Enum.Parse(typeof(TipoUsuario), cliTmp[22].ToString()),
                    contraseña = cliTmp[23].ToString()


                };
                res.Add(cli);
            }
            //regresamos la lista de cliente
            return res;
        }

        public List<Usuario> consultar(object criteriosBusqueda)
        {
            //lista de object para resultado
            List<Usuario> res = new List<Usuario>();
            //hacemos el where
            string where = "";
            //-------------------------------------------REVISAR ESTO -----------------------------------------
        /*    for (int i = 0; i < criteriosBusqueda.Count; i++)
                where += " " + criteriosBusqueda[i].campo + " " + criteriosBusqueda[i].opIntermedioSql() + " " + criteriosBusqueda[i].valor + " " + criteriosBusqueda[i].opFinalSql() + " ";
*/            //hacemos la consulta
            List<object> tmp = bd.consultar("*", "Usuarios", where);
            //mapeamos cada Object en un Usuario
            foreach (object[] cliTmp in tmp)
            {
                Domicilio dom = new Domicilio();
                dom.calle = cliTmp[8].ToString();
                dom.numero = cliTmp[9].ToString();
                dom.codigoPostal = cliTmp[10].ToString();
                dom.colonia = cliTmp[11].ToString();
                dom.seccionFraccionamiento = cliTmp[12].ToString();
                dom.localidad = cliTmp[13].ToString();
                dom.municipio = cliTmp[14].ToString();
                dom.fotoComprobante = cliTmp[15].ToString();
                object cli = new
                {
                    Id = int.Parse(cliTmp[0].ToString()),
                    Nombre = cliTmp[1].ToString(),
                    ApellidoPaterno = cliTmp[2].ToString(),
                    ApellidoMaterno = cliTmp[3].ToString(),
                    FechaNacimiento = DateTime.Parse(cliTmp[4].ToString()),
                    Celular = cliTmp[5].ToString(),
                    Telefono = cliTmp[6].ToString(),
                    Correo = cliTmp[7].ToString(),
                    Domicilio = dom,
                    ComprobanteINE = cliTmp[16].ToString(),
                    Curp = cliTmp[17].ToString(),
                    CurpCompro = cliTmp[18].ToString()
                };
                res.Add((Usuario)cli);
            }
            //regresamos la lista de Usuario
            return res;

        }

        /// <summary>
        /// Autenticaci{on de Usuario, mediante correo y constraseña.
        /// Si no se encuentra, llega TipoUsuario.ERROR!
        /// </summary>
        /// <param name="us">Correo del usuario a loguear</param>
        /// <param name="contraseña">Contraseña del usuario</param>
        /// <returns>TipoUsuario.ADMINISTRADOR, CAJERO o ERROR, según sea el caso</returns>
        public TipoUsuario login(string us, string contraseña)
        {
            object usLogueado = bd.consultarUnRegistro("*", "usuarios", " correo='" + us + "' AND pwd='" + contraseña + "'");
            object[] user = (object[])usLogueado;
            
            //verificamos resultado
            if (usLogueado != null)
            {
                string tipo = user[22].ToString();
                if (tipo == "ADMINISTRADOR")
                    return TipoUsuario.ADMINISTRADOR;
                else if (tipo == "CAJERO")
                    return TipoUsuario.CAJERO;
                else return TipoUsuario.ERROR;
            }
            else
            {
                msgError = "Usuario NO REGISTRADO. Consulte a Administrador. ";
                return TipoUsuario.ERROR;
            }
        }

        public bool modificar(List<DatosParaActualizar> datos, int id)
        {
            string camposValores = "";
            for (int i = 0; i < datos.Count; i++)
            {
                camposValores += " " + datos[i].campo + " = " + "'" +datos[i].valor+"'";
                if (i < datos.Count - 1) camposValores += ",";
            }
            //ejecuta el actualizar de BD con los datos
            return bd.actualizar("usuarios", camposValores, "id=" + id);
            //regresar el res
        }

        public List<T> consultar<T>(List<CriteriosBusqueda> criteriosBusqueda)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Autenticaci{on de Usuario, mediante correo y constraseña.
        /// Si no se encuentra, llega TipoUsuario.ERROR!
        /// </summary>
        /// <param name="us">Correo del usuario a loguear</param>
        /// <param name="contraseña">Contraseña del usuario</param>
        /// <returns>TipoUsuario.ADMINISTRADOR, CAJERO o ERROR, según sea el caso</returns>
        /// 

            ///  Codigo para configurar para jalar el nombre de los usuarios
        public Usuario loginUsuario(string us, string contraseña)
        {
            object usLogueado = bd.consultarUnRegistro("*", "usuarios", " correo='" + us + "' AND pwd='" + contraseña + "'");
            object[] user = (object[])usLogueado;

            //verificamos resultado
            if (usLogueado != null)
            {
                Domicilio dom = new Domicilio();
                dom.calle = user[8].ToString();
                dom.numero = user[9].ToString();
                dom.codigoPostal = user[10].ToString();
                dom.colonia = user[11].ToString();
                dom.seccionFraccionamiento = user[12].ToString();
                dom.localidad = user[13].ToString();
                dom.municipio = user[14].ToString();
                dom.fotoComprobante = user[15].ToString();
                Usuario res = new Usuario
                {
                    Id = int.Parse(user[0].ToString()),
                    Nombre = user[1].ToString(),
                    ApellidoPaterno = user[2].ToString(),
                    ApellidoMaterno = user[3].ToString(),
                    FechaNacimiento = DateTime.Parse(user[4].ToString()),
                    Celular = user[5].ToString(),
                    Telefono = user[6].ToString(),
                    Correo = user[7].ToString(),
                    Domicilio = dom,
                    ComprobanteINE = user[16].ToString(),
                    Curp = user[17].ToString(),
                    CurpCompro = user[18].ToString()
                };
                return res;
            }
            else
            {
                msgError = "Error al logear usuario NO REGISTRADO. Consulte a Administrador. ";
                return null;
            }
        }
    }
}