﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib_pdv_uth_v1.usuarios
{
    public class DatosPersonasSecundarias
    {
        //Propiedades de persona alternativa
        private int id;
        private string nombre;
        private string apellidoPaterno;
        private string apellidoMaterno;
        private DateTime fechaNacimiento;
        private string celular;
        private string telefono;
        private string correo;
        private string calle;
        private string numCasa;
        private string codigoPostal;
        private string colonia;
        private string fraccionamiento;
        private string localidad;
        private string municipio;
        private string comproDomicilio;
        private string comprobanteINE;
        private string curp;
        private string comproCURP;
        private int clienteID;

        //Constructor de Persona Alternativa
        public DatosPersonasSecundarias(int id, string nombre, string apellidoPaterno, string apellidoMaterno, DateTime fechaNacimiento, string celular, string telefono, string correo, string calle, string numCasa, string codigoPostal, string colonia, string fraccionamiento, string localidad, string municipio, string comproDomicilio, string comprobanteINE, string curp, string comproCURP, int clienteID)
        {
            this.id = id;
            this.nombre = nombre;
            this.apellidoPaterno = apellidoPaterno;
            this.apellidoMaterno = apellidoMaterno;
            this.fechaNacimiento = fechaNacimiento;
            this.celular = celular;
            this.telefono = telefono;
            this.correo = correo;
            this.calle = calle;
            this.numCasa = numCasa;
            this.codigoPostal = codigoPostal;
            this.colonia = colonia;
            this.fraccionamiento = fraccionamiento;
            this.localidad = localidad;
            this.municipio = municipio;
            this.comproDomicilio = comproDomicilio;
            this.comprobanteINE = comprobanteINE;
            this.curp = curp;
            this.comproCURP = comproCURP;
            this.clienteID = clienteID;
        }

        //Getters y setters de Persona Alternativa
        public int Id { get => id; set => id = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string ApellidoPaterno { get => apellidoPaterno; set => apellidoPaterno = value; }
        public string ApellidoMaterno { get => apellidoMaterno; set => apellidoMaterno = value; }
        public DateTime FechaNacimiento { get => fechaNacimiento; set => fechaNacimiento = value; }
        public string Celular { get => celular; set => celular = value; }
        public string Telefono { get => telefono; set => telefono = value; }
        public string Correo { get => correo; set => correo = value; }
        public string Calle { get => calle; set => calle = value; }
        public string NumCasa { get => numCasa; set => numCasa = value; }
        public string CodigoPostal { get => codigoPostal; set => codigoPostal = value; }
        public string Colonia { get => colonia; set => colonia = value; }
        public string Fraccionamiento { get => fraccionamiento; set => fraccionamiento = value; }
        public string Localidad { get => localidad; set => localidad = value; }
        public string Municipio { get => municipio; set => municipio = value; }
        public string ComproDomicilio { get => comproDomicilio; set => comproDomicilio = value; }
        public string ComprobanteINE { get => comprobanteINE; set => comprobanteINE = value; }
        public string Curp { get => curp; set => curp = value; }
        public string ComproCURP { get => comproCURP; set => comproCURP = value; }
        public int ClienteID { get => clienteID; set => clienteID = value; }
    }
}