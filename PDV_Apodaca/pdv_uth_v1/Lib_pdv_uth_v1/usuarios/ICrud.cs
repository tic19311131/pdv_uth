
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lib_pdv_uth_v1.usuarios
{
    public interface ICrud
    {
        /// <summary>
        /// el insert de cada tipo de objeto
        /// </summary>
        /// <returns>true si se guarda, flase si hay error</returns>
        bool alta();
        /// <summary>
        /// El UPDATE de cada tipo de objeto
        /// </summary>
        /// <param name="datos">Los pares de campo=valor</param>
        /// <param name="id">el id del registro que quiero actualizar</param>
        /// <returns>true si se actualiza, false si hay error</returns>
        bool modificar(List<DatosParaActualizar> datos, int id);
        /// <summary>
        /// Elimina registros que cumplan con el ID
        /// </summary>
        /// <param name="id">Id del registro a eliminar</param>
        /// <returns>treu si se elimina y false si hay error</returns>
        bool eliminar(int id);

        /// <summary>
        /// Permite obtener registros resultantes de unSELECT
        /// </summary>
        /// <typeparam name="T">El tipo de objeto que quiero consultar (Cliente, Producto, Usuario, etc...)</typeparam>
        /// <param name="criteriosBusqueda">Lista de objs que tienen campo, valor y operador</param>
        /// <returns>Lista de objetos del TIPO que se haya definido 'T'</returns>
        List<object> consultar(List<CriteriosBusqueda> criteriosBusqueda);

    }
}