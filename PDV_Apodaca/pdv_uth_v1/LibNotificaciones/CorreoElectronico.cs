﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace LibNotificaciones
{
    public class CorreoElectronico
    {
        public static string errorMsg = "";
        //El servicio smtp

        //Instanciamos SmtpClient
        SmtpClient smtp = new SmtpClient();
        //Create the mail message
        MailMessage mail = new MailMessage();
        //Datos del admin
        String emailAdminPDV = "tic19311131@uthermosillo.edu.mx";
        String emailPassword = "20uthespinoza";
        public CorreoElectronico()
        {
            //Preparamos el smtp
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;//25;
            smtp.EnableSsl = true;
            smtp.Credentials = new NetworkCredential(emailAdminPDV, emailPassword);
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
        }

        public bool notificar(string destinatario, string asunto, string mensaje)
        {
            bool res = false;
            try
            {
                //set the address
                mail.From = new MailAddress(this.emailAdminPDV, "Soporte - PDV UTH");
                mail.To.Add(destinatario);
                //Establece el asunto
                mail.Subject = asunto;
                //msg
                mail.Body = mensaje;
                //send the mail
                smtp.Send(mail);
                //Cambiamos el res a true
                res = true;

            }
            catch (Exception ex)
            {

                //Capturar el mensaje de error
                errorMsg = "Error al mandar correo a <" + destinatario + ">. " + ex.Message;
            }
            return res;
        }
    }
}
